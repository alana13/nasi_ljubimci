package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.formbean.DogadajiForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class DogadajDAO {

	@Autowired
    private EntityManager entityManager;
	
    private static String tableName = "dogadaj";
    
    public boolean stvoriDogadaj( String username, DogadajiForm form) {
    	
    	try {
            String sql = "insert into " + tableName + " (datum_pocetak, datum_kraj, lokacija, opis, privpubl, username) values (?, ?, ?, ?, ?, ?)";
            
            System.out.println( form);
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, form.getStartDate())
            .setParameter(2, form.getEndDate())
            .setParameter(3, form.getLokacija())
            .setParameter(4, form.getOpis())
            .setParameter(5, form.isPrivpubl() != null ? form.isPrivpubl() : false)
            .setParameter(6, username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	
    	return true;
    }

	public List<DogadajENTITY> dajDogadaje(String username) {
		String sql = "Select e from " + DogadajENTITY.class.getName() + " e " //
                + " Where e.Korisnik.userName = :arg ";
        
    	Query query = entityManager.createQuery(sql, DogadajENTITY.class);
        query.setParameter("arg", username);
        
        return (List<DogadajENTITY>) query.getResultList();
	}
	
	public int najveciId() {
		try {
            String sql = "select max( id) from " + tableName;
            
            return (int) entityManager.createNativeQuery( sql).getSingleResult();
        } catch (NoResultException e) {
            return -1;
        }
	}

	public List<DogadajENTITY> dajJavneDogadaje(String username) {
		String sql = "Select e from " + DogadajENTITY.class.getName() + " e " //
                + " Where e.Korisnik.userName = :arg and privpubl = false ";
        
    	Query query = entityManager.createQuery(sql, DogadajENTITY.class);
        query.setParameter("arg", username);
        
        return (List<DogadajENTITY>) query.getResultList();
	}
}
