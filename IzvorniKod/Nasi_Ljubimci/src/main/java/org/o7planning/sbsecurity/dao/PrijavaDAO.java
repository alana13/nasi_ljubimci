package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.PorukaENTITY;
import org.o7planning.sbsecurity.entity.PrijavaENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PrijavaDAO {

	@Autowired
	private EntityManager entityManager;

	private static String tableName = "prijava";

	public List<PrijavaENTITY> dohvatiPrijave() {
		String sql = "SELECT e FROM " + PrijavaENTITY.class.getName() + " e ORDER BY e.datum ASC";

		Query query = entityManager.createQuery(sql, PrijavaENTITY.class);

		return (List<PrijavaENTITY>) query.getResultList();
	}

	public boolean izbrisiPrijavu(int iDPrijave) {
		try {
			String sql = "DELETE FROM " + PrijavaENTITY.class.getName() + " e WHERE e.id = :iDPrijave";
			entityManager.createQuery(sql).setParameter("iDPrijave", iDPrijave).executeUpdate();
			return true;
		} catch (NoResultException e) {
			return false;
		}
	}

	public boolean stvoriPrijavu(String usernamePrijavitelja, String usernamePrijavljenog, String opisPrijave) {
		String sql = "insert into "+ tableName +" ( username_prijavitelja, username_prijavljenog, opis) "
				+ " values (?, ?, ?)";
		
		entityManager.createNativeQuery(sql)
			.setParameter(1, usernamePrijavitelja)
			.setParameter(2, usernamePrijavljenog)
			.setParameter(3, opisPrijave)
			.executeUpdate();
		
		return true;
	}
}