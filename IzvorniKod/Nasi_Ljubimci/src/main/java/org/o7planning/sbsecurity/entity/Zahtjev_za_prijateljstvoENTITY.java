package org.o7planning.sbsecurity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zahtjev_za_prijateljstvo")
public class Zahtjev_za_prijateljstvoENTITY {
	
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "username_1", nullable = false)
	private OsobaENTITY osoba1;

	@ManyToOne
	@JoinColumn(name = "username_2", nullable = false)
	private OsobaENTITY osoba2;
	
	@Column(name="status", nullable=false)
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OsobaENTITY getOsoba1() {
		return osoba1;
	}

	public void setOsoba1(OsobaENTITY osoba1) {
		this.osoba1 = osoba1;
	}

	public OsobaENTITY getOsoba2() {
		return osoba2;
	}

	public void setOsoba2(OsobaENTITY osoba2) {
		this.osoba2 = osoba2;
	}

}
