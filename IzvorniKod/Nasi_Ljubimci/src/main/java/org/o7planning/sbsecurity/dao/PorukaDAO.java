package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.PorukaENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PorukaDAO {

	@Autowired
    private EntityManager entityManager;
	
	private static String tableName = "poruka";
	
	public boolean pohraniPoruku( String posiljatelj, String primatelj, String sadrzaj) {
		try {
            String sql = "insert into " + tableName +
            		" (sadrzaj, posiljatelj_username, primatelj_username) values(?, ?, ?)";
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, sadrzaj)
            .setParameter(2, posiljatelj)
            .setParameter(3, primatelj)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}

	public List<PorukaENTITY> dohvatiPoruke(String user1, String user2) {
		String sql = "Select e from " + PorukaENTITY.class.getName() + " e " //
                + " WHERE e.posiljatelj.userName = :user1 AND e.primatelj.userName = :user2 OR "
                + " e.posiljatelj.userName = :user2 AND e.primatelj.userName = :user1 "
                + " ORDER BY e.datum ASC";
        
    	Query query = entityManager.createQuery(sql, PorukaENTITY.class);
        query.setParameter("user1", user1);
        query.setParameter("user2", user2);
        
        return (List<PorukaENTITY>) query.getResultList();
	}

	public List<String> getKorisnike(String username) {
		String sql = " Select posiljatelj_username from " + tableName + " " 
                + " WHERE primatelj_username = ? "
				+ " UNION " 
				+ " Select primatelj_username from " + tableName + " " 
                + " WHERE posiljatelj_username = ? ";
        
    	Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, username);
        query.setParameter(2, username);
        
        return (List<String>) query.getResultList();
	}
}
