package org.o7planning.sbsecurity.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.Tuple;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class Kor_kom_dogDAO {

	@Autowired
    private EntityManager entityManager;
	
	private static String tableName = "kor_kom_dog";
	
	public boolean spremiKomentar(String username, int idDog, int idKom) {
		try {
			String sql = "insert into " + tableName + " (username, id_dog, id_kom) values(?, ?, ?)";
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, username)
            .setParameter(2, idDog)
            .setParameter(3, idKom)
            .executeUpdate();
        } catch(NoResultException e) {
            return false;
        }
    	return true;
	}
	
	public List<Kor_kom_dogENTITY> dajSveKomentareNaDogađaje(List<DogadajENTITY> dogadaji){
		List<Integer> dogadajiId = dogadaji.stream().map(DogadajENTITY::getId).collect(Collectors.toList());
		
		String sql = "Select e from " + Kor_kom_dogENTITY.class.getName() + " e " //
                + " Where e.Dogadaj.id IN :argList ";
        
    	Query query = entityManager.createQuery(sql, Kor_kom_dogENTITY.class);
        query.setParameter("argList", dogadajiId);
       
        return (List<Kor_kom_dogENTITY>) query.getResultList();
	}
}
