package org.o7planning.sbsecurity.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import org.o7planning.sbsecurity.dao.LjubimacDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.Profil_ljubimcaENTITY;
import org.o7planning.sbsecurity.formbean.LjubimacForm;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/ljubimac")
public class LjubimacController {

	@Autowired
	LjubimacDAO ljubimacDAO;
	
	@Autowired
	ObjavaDAO objavaDAO;

	@RequestMapping(value = { "/profil/{id}", "/{id}" }, method = RequestMethod.GET)
	public String ljubimacInfo(Model model, Principal principal, @PathVariable("id") Integer id) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		Profil_ljubimcaENTITY ljubimac = ljubimacDAO.findLjubimacById(id);
		if (ljubimac == null) {
			System.out.println("Ljubimac ne postoji");
			return ("redirect:/vlasnik/profil");
		}
		List<ObjavaENTITY> objaveLjubimca= objavaDAO.dajSveObjaveLjubimca(osoba.getUsername(), id) ;
		model.addAttribute("ljubimac",ljubimac);
		model.addAttribute("objave",objaveLjubimca);
		return ("ljubimacProfile");

	}

	@RequestMapping(value = { "/profil/{id}", "/{id}" }, method = RequestMethod.POST)
	public String azuiriranje_ljubimca(Model model, Principal principal, @PathVariable("id") Integer id,
			@ModelAttribute("LjubimacForm") LjubimacForm ljubimacForm,
			@RequestParam("profilna_fotografija") MultipartFile file) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();

		if (!file.isEmpty()) {
			byte[] data = null;
			String contentType = "";
			try {
				data = file.getBytes();
				contentType = file.getContentType();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (ljubimacDAO.updateLjubimac(ljubimacForm, data, contentType, osoba.getUsername(), id) == true) {
				System.out.println("Ljubimac je azuriran");
			} else
				System.out.println("Ljubimac nije azuriran");
		}
		else {
			System.out.println("Here we are");
			if (ljubimacDAO.updateLjubimacBezSlike(ljubimacForm, osoba.getUsername(), id) == true) {
				System.out.println("Ljubimac je azuriran");
			} else
				System.out.println("Ljubimac nije azuriran");
		}
			

		return "redirect:/ljubimac/profil/" + id;
	}
	
	@RequestMapping(value = "/dajContent{id}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageAsByteArray1( @PathVariable("id") Integer id) throws IOException {
		Profil_ljubimcaENTITY ljubimac = ljubimacDAO.findLjubimacById(id);
	    return ResponseEntity.ok().contentType( MediaType.valueOf( ljubimac.getProfilna_fotografija())).body( ljubimac.getData());
	}

}
