package org.o7planning.sbsecurity.formbean;

public class AppTvrtkaForm {
	
    private String username;
    private String password;
    private String confirmPassword;
    private String address;
    private boolean enabled;
    private String name;
    private String contact;
 
	public AppTvrtkaForm(String username, String password, String confirmPassword, String address, 
			boolean enabled, String name, String contact) {
		super();
		this.username = username;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.address = address;
		this.enabled = enabled;
		this.name = name;
		this.contact = contact;
	}
	
	public AppTvrtkaForm() {}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
    
    
}
