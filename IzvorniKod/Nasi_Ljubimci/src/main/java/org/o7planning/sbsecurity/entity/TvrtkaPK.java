package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class TvrtkaPK implements Serializable{

	private KorisnikENTITY Korisnik;

	

	public TvrtkaPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY Korisnik) {
		this.Korisnik = Korisnik;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Korisnik);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TvrtkaPK other = (TvrtkaPK) obj;
		return Objects.equals(Korisnik, other.Korisnik);
	}



	
}
