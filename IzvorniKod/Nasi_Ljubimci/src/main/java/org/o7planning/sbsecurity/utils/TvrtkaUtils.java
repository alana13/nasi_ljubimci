package org.o7planning.sbsecurity.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaUslugaENTITY;
import org.o7planning.sbsecurity.formbean.TvrtkaUpdateForm;

public class TvrtkaUtils {

    public static Map<Integer, List<Kor_kom_objENTITY>> komentarIzObjektaUString( List<Kor_kom_objENTITY> komentari){
    	return komentari.stream()
    			.collect( Collectors.groupingBy(
    					e -> e.getObjava().getId(), 
    					Collectors.mapping( e -> {return e;}, Collectors.toList())));
    }
	
	
	public static Map<String, String> mapirajUINformacije( TvrtkaENTITY tvrtka, List<TvrtkaUslugaENTITY> usluge){
		Map<String, String> informacije = new LinkedHashMap<>();
		informacije.put( "username", tvrtka.getKorisnik().getUserName());
		informacije.put( "adresa", tvrtka.getAdresa());
		informacije.put( "opis", tvrtka.getOpis());
		informacije.put( "naziv", tvrtka.getNaziv());
		informacije.put( "kontakt", tvrtka.getKontakt());
		
		for( TvrtkaUslugaENTITY e : usluge) {
			if( e.getUsluga().getId() == 1) {
				// sluga cuvanje
				informacije.put( "cuvanje", e.getOpis());
			}else if( e.getUsluga().getId() == 2) {
				// usluga odgoj
				informacije.put( "odgoj", e.getOpis());
			}else if( e.getUsluga().getId() == 3) {
				// usluga briga o zdravlju
				informacije.put( "briga o zdravlju", e.getOpis());
			}
		}
		
		return informacije;
	}
	
	public static void popuniTvrtkaUpdateForm( Map<String, String> informacije, TvrtkaUpdateForm form) {
		form.setAdresa( informacije.get( "adresa"));
		form.setOpis( informacije.get( "opis"));
		form.setNaziv( informacije.get( "naziv"));
		form.setKontakt( informacije.get( "kontakt"));
		
		if( informacije.containsKey( "cuvanje")) {
    		form.setCuvanje( true);
    		form.setOpisCuvanje( informacije.get( "cuvanje"));
    	}
		
		if( informacije.containsKey( "odgoj")) {
    		form.setOdgoj( true);
    		form.setOpisOdgoj( informacije.get( "odgoj"));
    	}
		
		if( informacije.containsKey( "briga o zdravlju")) {
    		form.setBrigaOZdravlju( true);
    		form.setOpisBrigaOZdravlju( informacije.get( "briga o zdravlju"));
    	}
	}
	
}
