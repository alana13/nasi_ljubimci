package org.o7planning.sbsecurity.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.dao.DogadajDAO;
import org.o7planning.sbsecurity.dao.KomentarDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_dogDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_objDAO;

import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.PorukaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaDAO;
import org.o7planning.sbsecurity.dao.UserRoleDAO;
import org.o7planning.sbsecurity.entity.PorukaENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.o7planning.sbsecurity.formbean.AppTvrtkaForm;
import org.o7planning.sbsecurity.formbean.DogadajiForm;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.o7planning.sbsecurity.validator.OsobaValidator;
import org.o7planning.sbsecurity.validator.TvrtkaValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private UserRoleDAO userRoleDAO;

	@Autowired
	private OsobaDAO osobaDAO;

	@Autowired
	private TvrtkaDAO tvrtkaDAO;

	@Autowired
	private OsobaValidator osobaValidator;

	@Autowired
	private TvrtkaValidator tvrtkaValidator;

	// Set a form validator
	@InitBinder
	protected void initBinder(WebDataBinder dataBinder) {
		// Form target
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == AppOsobaForm.class) {
			dataBinder.setValidator(osobaValidator);
		} else if (target.getClass() == AppTvrtkaForm.class) {
			dataBinder.setValidator(tvrtkaValidator);
		}
		// ...
	}

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String welcomePage(Model model) {
		model.addAttribute("title", "Welcome");
		model.addAttribute("message", "This is welcome page!");
		return "welcomePage";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
		return "loginPage";
	}

	@RequestMapping(value = "/registerOsoba", method = RequestMethod.GET)
	public String viewRegisterOsoba(Model model) {
		AppOsobaForm form = new AppOsobaForm();
		model.addAttribute("appOsobaForm", form);
		return "registerOsobaPage";
	}

	@RequestMapping(value = "/registerOsoba", method = RequestMethod.POST)
	public String saveRegisterOsoba(Model model, //
			@ModelAttribute("appOsobaForm") @Validated AppOsobaForm appOsobaForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		// Validate result
		if (result.hasErrors()) {
			return "registerOsobaPage";
		}

		try {
			// stvori zapis osobe
			korisnikDAO.createKorisnik(appOsobaForm.getUsername(), appOsobaForm.getPassword());
			userRoleDAO.createUserRole(appOsobaForm.getUsername(), 2);
			osobaDAO.createOsoba(appOsobaForm);
		}
		// Other error!!
		catch (Exception e) {
			model.addAttribute("errorMessage", "Error: " + e.getMessage());
			return "registerOsobaPage";
		}

		return "redirect:/preusmjeriProfil";
	}

	@RequestMapping(value = "/registerTvrtka", method = RequestMethod.GET)
	public String viewRegisterTvrtka(Model model) {
		AppTvrtkaForm form = new AppTvrtkaForm();
		model.addAttribute("appTvrtkaForm", form);
		return "registerTvrtkaPage";
	}

	@RequestMapping(value = "/registerTvrtka", method = RequestMethod.POST)
	public String saveRegisterTvrtka(Model model, //
			@ModelAttribute("appTvrtkaForm") @Validated AppTvrtkaForm appTvrtkaForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		// Validate result
		if (result.hasErrors()) {
			return "registerTvrtkaPage";
		}

		try {
			// stvori zapis tvrtke
			korisnikDAO.createKorisnik(appTvrtkaForm.getUsername(), appTvrtkaForm.getPassword());
			userRoleDAO.createUserRole(appTvrtkaForm.getUsername(), 3);
			tvrtkaDAO.createOsoba(appTvrtkaForm);
		}
		// Other error!!
		catch (Exception e) {
			model.addAttribute("errorMessage", "Error: " + e.getMessage());
			return "registerTvrtkaPage";
		}

		return "redirect:/preusmjeriProfil";
	}

	@RequestMapping(value = "/preusmjeriProfil", method = RequestMethod.GET)
	public String preusmjeriProfil(Model model, Principal principal) {
		
		// nakon uspjenog logina preusmjeri korisnika na pravi kontroler
		User loginedUser = (User) ((Authentication) principal).getPrincipal();
		if (loginedUser.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_TVRTKA"))) {
			return "redirect:/tvrtka/profil";
		}
		if (loginedUser.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_VLASNIK"))) {
			return "redirect:/vlasnik/profil";
		}
		if (loginedUser.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
			return "redirect:/administrator/profil";
		}

		return "errorPage";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model, Principal principal) {

		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();

			String userInfo = WebUtils.toString(loginedUser);

			model.addAttribute("userInfo", userInfo);

			String message = "Hi " + principal.getName() //
					+ "<br> You do not have permission to access this page!";
			model.addAttribute("message", message);
		}

		return "403Page";
	}	
}
