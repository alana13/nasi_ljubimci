package org.o7planning.sbsecurity.utils;

import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.formbean.VlasnikUpdateForm;

public class OsobaUtils {

	public static void popuniTvrtkaUpdateForm(OsobaENTITY podaci, VlasnikUpdateForm form) {
		form.setAdresa(podaci.getAdresa());
		form.setEmail(podaci.getEmail());
		form.setOpis(podaci.getOpis());
	}

}
