package org.o7planning.sbsecurity.dao;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.KorisnikENTITY;
import org.o7planning.sbsecurity.entity.Profil_ljubimcaENTITY;
import org.o7planning.sbsecurity.formbean.LjubimacForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class LjubimacDAO {
	
	@Autowired
    private EntityManager entityManager;
	
    private static String tableName = "Profil_ljubimca";
    
    public Profil_ljubimcaENTITY findLjubimacById(int id) {
    	
    	try {
            String sql = "Select e from " +  Profil_ljubimcaENTITY.class.getName() + " e " //
                    + " Where e.id = :id ";

            Query query = entityManager.createQuery(sql, Profil_ljubimcaENTITY.class);
            query.setParameter("id", id);

            return (Profil_ljubimcaENTITY) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    	
    }
    
    public boolean createLjubimac(LjubimacForm ljubimac,byte[] data,String contentType, String username) {
    	try {
            String sql = "insert into " + tableName + " (vrsta, ime, datum_rodenja, spol, profilna_fotografija, data, opis, username) values (?, ?, ?, ?, ?, ?, ?, ?)";
            
            System.out.println(ljubimac.getDatum_rodenja().toString());
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, ljubimac.getVrsta())
            .setParameter(2, ljubimac.getIme())
            .setParameter(3, ljubimac.getDatum_rodenja())
            .setParameter(4, ljubimac.getSpol())
            .setParameter(5, contentType)
            .setParameter(6, data)
            .setParameter(7, ljubimac.getOpis())
            .setParameter(8, username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    	
    }
    
    public boolean updateLjubimac(LjubimacForm ljubimac,byte[] data,String contentType, String username, int id) {
    	
    
    	try {
            String sql = "update " + Profil_ljubimcaENTITY.class.getSimpleName() + " e SET e.ime= :ime, e.datum_rodenja= :datum_rodenja, e.profilna_fotografija= :profilna_fotografija, e.data= :data, e.opis= :opis where e.Osoba.korisnik.userName= :username AND e.id= :id";
            
            System.out.println(ljubimac.getDatum_rodenja().toString());
            entityManager.createQuery( sql)
            .setParameter("ime", ljubimac.getIme())
            .setParameter("datum_rodenja", ljubimac.getDatum_rodenja())
            .setParameter("profilna_fotografija", contentType)
            .setParameter("data", data)
            .setParameter("opis", ljubimac.getOpis())
            .setParameter("username", username)
            .setParameter("id", id)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    }
    
    public boolean updateLjubimacBezSlike(LjubimacForm ljubimac,String username, int id) {
    	
        
    	try {
            String sql = "update " + Profil_ljubimcaENTITY.class.getSimpleName() + " e SET  e.ime= :ime, e.datum_rodenja= :datum_rodenja,  e.opis= :opis where e.Osoba.korisnik.userName= :username AND e.id= :id";
            
            System.out.println(ljubimac.getDatum_rodenja().toString());
            
            entityManager.createQuery( sql)
            .setParameter("ime", ljubimac.getIme())
            .setParameter("datum_rodenja", ljubimac.getDatum_rodenja())
            .setParameter("opis", ljubimac.getOpis())
            .setParameter("username", username)
            .setParameter("id", id)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    }
    
    public List<Profil_ljubimcaENTITY> listAllLjubimci(String username){
    	try {
            String sql = "Select e from " + Profil_ljubimcaENTITY.class.getName() + " e " //
                    + " Where e.Osoba.korisnik.userName = :username order by e.id asc";

            Query query = entityManager.createQuery(sql, Profil_ljubimcaENTITY.class);
            query.setParameter("username", username);

            return (List<Profil_ljubimcaENTITY>) query.getResultList();
            
        } catch (NoResultException e) {
            return null;
        }
    	
    	
    }
    
    public boolean ObrisiLjubimca(int id) {
    	try {
            String sql = "Delete from " + Profil_ljubimcaENTITY.class.getName() + " e " //
                    + " Where e.id = :id";

            entityManager.createQuery(sql).setParameter("id", id).executeUpdate();
            
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    }
    

}
