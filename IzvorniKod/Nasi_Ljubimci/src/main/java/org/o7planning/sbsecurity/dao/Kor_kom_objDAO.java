package org.o7planning.sbsecurity.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.Tuple;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class Kor_kom_objDAO {

	@Autowired
    private EntityManager entityManager;
	
	private static String tableName = "kor_kom_obj";
	
	public boolean spremiKomentar( String username, int idObj, int idKom) {
		try {
            String sql = "insert into " + tableName + " (username, id_obj, id_kom) values(?, ?, ?)";
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, username)
            .setParameter(2, idObj)
            .setParameter(3, idKom)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}
	
	public List<Kor_kom_objENTITY> dajSveKomentareNaObjave( List<ObjavaENTITY> objave){
		List<Integer> objaveId = objave.stream().map( ObjavaENTITY::getId).collect( Collectors.toList());
		
		String sql = "Select e from " + Kor_kom_objENTITY.class.getName() + " e " //
                + " Where e.Objava.id IN :argList ";
        
    	Query query = entityManager.createQuery(sql, Kor_kom_objENTITY.class);
        query.setParameter("argList", objaveId);
       
        return (List<Kor_kom_objENTITY>) query.getResultList();
	}
}
