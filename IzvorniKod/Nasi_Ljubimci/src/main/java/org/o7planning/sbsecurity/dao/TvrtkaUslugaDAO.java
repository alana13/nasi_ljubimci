package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaUslugaENTITY;
import org.o7planning.sbsecurity.formbean.TvrtkaUpdateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TvrtkaUslugaDAO {
	@Autowired
	private EntityManager entityManager;

	private static String tableName = "tvrtka_usluga";
	
	public List<TvrtkaUslugaENTITY> dajUsluge( String username){
		String sql = "select e from " + TvrtkaUslugaENTITY.class.getName() + " e " //
                + " Where e.tvrtka.Korisnik.userName = :arg ";
        
    	Query query = entityManager.createQuery(sql, TvrtkaUslugaENTITY.class);
        query.setParameter("arg", username);
        
        return (List<TvrtkaUslugaENTITY>) query.getResultList();
	}
	
	public boolean pobrisiUsluge( String username) {
		try {
            String sql = "delete from " + TvrtkaUslugaENTITY.class.getName() +
            		" e  where e.tvrtka.Korisnik.userName = :username";
            
            entityManager.createQuery( sql)
            .setParameter("username", username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}
	
	public boolean zapisiUsluge( String username, TvrtkaUpdateForm form) {
		// nema nikakvih usluga pa ne treba nista zapisat
		if( !form.isBrigaOZdravlju() && !form.isCuvanje() && !form.isOdgoj()) return true;
		try {
			
			String values = "";
			if( form.isCuvanje()) {
				values += "(1, '" + username + "', '" + form.getOpisCuvanje() + "'),";
			}
			if( form.isOdgoj()) {
				values += "(2, '" + username + "', '" + form.getOpisOdgoj() + "'),";
			}
			if( form.isBrigaOZdravlju()) {
				values += "(3, '" + username + "', '" + form.getOpisBrigaOZdravlju() + "'),";
			}
			values = values.substring(0, values.length()-1); // ukloni ',' na kraju
			
            String sql = "insert into " + tableName +" (uslugaid, username, opis)" +
            		" values " + values;
			
            entityManager.createNativeQuery(sql)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}
}
