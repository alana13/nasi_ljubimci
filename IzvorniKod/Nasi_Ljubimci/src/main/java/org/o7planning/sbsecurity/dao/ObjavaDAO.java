package org.o7planning.sbsecurity.dao;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.Tag_ljubimcaENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ObjavaDAO{
	
	@Autowired
    private EntityManager entityManager;
	
    private static String tableName = "objava";
    
    public boolean spremiObjavuPoruka( String username, String poruka,  boolean privILiPublic) {
    	try {
            String sql = "insert into " + tableName + " (opis, datum, slika_ili_video, data, privpubl, username) values(?, ?, NULL, NULL, ?, ?)";
            Date date = new Date( Calendar.getInstance().getTime().getTime());
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, poruka)
            .setParameter(2, date)
            .setParameter(3, privILiPublic)
            .setParameter(4, username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    }
    
    public boolean spremiObjavuPorukaSlikaVideo( String username, String poruka, byte[] data, String contentType, boolean privILiPublic) {
    	try {
            String sql = "insert into " + tableName + " (opis, datum, slika_ili_video, data, privpubl, username) values(?, ?, ?, ?, ?, ?)";
            
            Date date = new Date( Calendar.getInstance().getTime().getTime());
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, poruka)
            .setParameter(2, date)
            .setParameter(3, contentType)
            .setParameter(4, data)
            .setParameter(5, privILiPublic)
            .setParameter(6, username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
    }
    
    public ObjavaENTITY dajObjavu( int id) {
    	String sql = "Select e from " + ObjavaENTITY.class.getName() + " e " //
                + " Where e.id = :id ";
        
    	Query query = entityManager.createQuery(sql, ObjavaENTITY.class);
        query.setParameter("id", id);
        return (ObjavaENTITY) query.getSingleResult();
    }
    
    public List<ObjavaENTITY> dajSveObjaveObjava( String username){
    	String sql = "Select e from " + ObjavaENTITY.class.getName() + " e " //
                + " Where e.Korisnik.userName = :arg ";
        
    	Query query = entityManager.createQuery(sql, ObjavaENTITY.class);
        query.setParameter("arg", username);
        
        return (List<ObjavaENTITY>) query.getResultList();
    }
    
    public List<ObjavaENTITY> dajSveObjaveLjubimca(String username, int ljubimac){
    	String sql = "Select e from " + ObjavaENTITY.class.getName() + " e join " + Tag_ljubimcaENTITY.class.getName() //
                + " t on e.id=t.objava.id Where e.Korisnik.userName = :arg and t.profilLjubimca.id= :id";
        
    	Query query = entityManager.createQuery(sql, ObjavaENTITY.class);
        query.setParameter("arg", username).setParameter("id", ljubimac);
        
        return (List<ObjavaENTITY>) query.getResultList();
    }
    
    public boolean obrisiObjavu(int objavaId) {
    	try {
    		String sql1 = "delete from "+Kor_kom_objENTITY.class.getSimpleName()+ " e where e.Objava.id= :objavaId ";
			entityManager.createQuery(sql1)
					.setParameter("objavaId", objavaId)
					.executeUpdate();
			String sql2 = "delete from "+ObjavaENTITY.class.getSimpleName()+ " e where e.id= :objavaId ";
			entityManager.createQuery(sql2)
					.setParameter("objavaId", objavaId)
					.executeUpdate();
		} catch (NoResultException e) {
			return false;
		}

		return true;
    	
    }
    
    
    
}
