package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prijava")
public class PrijavaENTITY {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "serial")
	private int id;

	@Column(name = "datum", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date datum;

	@ManyToOne
	@JoinColumn(name = "username_prijavitelja", nullable = false)
	private KorisnikENTITY prijavitelj;

	@ManyToOne
	@JoinColumn(name = "username_prijavljenog", nullable = false)
	private KorisnikENTITY prijavljeni;

	@Column(name = "opis", nullable = true)
	private String opis;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public KorisnikENTITY getPrijavitelj() {
		return prijavitelj;
	}

	public void setPrijavitelj(KorisnikENTITY prijavitelj) {
		this.prijavitelj = prijavitelj;
	}

	public KorisnikENTITY getPrijavljeni() {
		return prijavljeni;
	}

	public void setPrijavljen(KorisnikENTITY prijavljeni) {
		this.prijavljeni = prijavljeni;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public void setPrijavljeni(KorisnikENTITY prijavljeni) {
		this.prijavljeni = prijavljeni;
	}

}