package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class KorisnikPK implements Serializable{

	
	private String userName;

	public KorisnikPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KorisnikPK other = (KorisnikPK) obj;
		return Objects.equals(userName, other.userName);
	}
	
}
