package org.o7planning.sbsecurity.models;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class Tvrtka extends User{
	
	public Tvrtka(String username, String password, Collection<? extends GrantedAuthority> authorities, boolean enabled) {
		super(username, password, enabled, true, true, true, authorities);
	}
}
