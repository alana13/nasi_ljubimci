package org.o7planning.sbsecurity.formbean;

import java.util.Map;

public class TvrtkaUpdateForm {

	private String adresa;
	private String opis;
	private String naziv;
	private String kontakt;
    
    private boolean cuvanje;
    private String opisCuvanje;
    private boolean odgoj;
    private String opisOdgoj;
    private boolean brigaOZdravlju;
    private String opisBrigaOZdravlju;
 
    public TvrtkaUpdateForm() {};
    
	public TvrtkaUpdateForm(String adresa, String opis, String naziv, String kontakt, boolean cuvanje,
			String opisCuvanje, boolean odgoj, String opisOdgoj, boolean brigaOZdravlju, String opisBrigaOZdravlju) {
		super();
		this.adresa = adresa;
		this.opis = opis;
		this.naziv = naziv;
		this.kontakt = kontakt;
		this.cuvanje = cuvanje;
		this.opisCuvanje = opisCuvanje;
		this.odgoj = odgoj;
		this.opisOdgoj = opisOdgoj;
		this.brigaOZdravlju = brigaOZdravlju;
		this.opisBrigaOZdravlju = opisBrigaOZdravlju;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getKontakt() {
		return kontakt;
	}

	public void setKontakt(String kontakt) {
		this.kontakt = kontakt;
	}

	public boolean isCuvanje() {
		return cuvanje;
	}

	public void setCuvanje(boolean cuvanje) {
		this.cuvanje = cuvanje;
	}

	public String getOpisCuvanje() {
		return opisCuvanje;
	}

	public void setOpisCuvanje(String opisCuvanje) {
		this.opisCuvanje = opisCuvanje;
	}

	public boolean isOdgoj() {
		return odgoj;
	}

	public void setOdgoj(boolean odgoj) {
		this.odgoj = odgoj;
	}

	public String getOpisOdgoj() {
		return opisOdgoj;
	}

	public void setOpisOdgoj(String opisOdgoj) {
		this.opisOdgoj = opisOdgoj;
	}

	public boolean isBrigaOZdravlju() {
		return brigaOZdravlju;
	}

	public void setBrigaOZdravlju(boolean brigaOZdravlju) {
		this.brigaOZdravlju = brigaOZdravlju;
	}

	public String getOpisBrigaOZdravlju() {
		return opisBrigaOZdravlju;
	}

	public void setOpisBrigaOZdravlju(String opisBrigaOZdravlju) {
		this.opisBrigaOZdravlju = opisBrigaOZdravlju;
	}
    
	
}
