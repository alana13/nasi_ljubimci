package org.o7planning.sbsecurity.formbean;

import java.util.Arrays;

import org.springframework.web.multipart.MultipartFile;

public class VlasnikUpdateForm {

	private MultipartFile profilnaFotografija;
	private String opis;
	private String email;
	private String adresa;

	public VlasnikUpdateForm() {

	}

	public VlasnikUpdateForm(MultipartFile profilnaFotografija, String opis, String email, String adresa) {
		this.profilnaFotografija = profilnaFotografija;
		this.opis = opis;
		this.email = email;
		this.adresa = adresa;
	}

	public MultipartFile getProfilnaFotografija() {
		return profilnaFotografija;
	}

	public void setProfilnaFotografija(MultipartFile profilnaFotografija) {
		this.profilnaFotografija = profilnaFotografija;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
}
