package org.o7planning.sbsecurity.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.time.ZoneOffset;
import java.util.Collections;

import org.o7planning.sbsecurity.formbean.DogadajiForm;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.Calendar.Events;
import com.google.api.services.calendar.Calendar.Events.List;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;

@Service
public class DogadajiServiceImpl {

	private static final String calendarId = "4u6fcgieu18iiphjbol80r6djg@group.calendar.google.com";
	private static final String APPLICATION_NAME = "serviceCal";
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private static com.google.api.services.calendar.Calendar client;

	private File keyFile = Path.of( "src/main/resources/progi-kalendar-a9149a81e7f9.p12").toFile();
	private Credential credential;

	private HttpTransport TRANSPORT;
	private String SERVICE_ACCOUNT = "nasi-ljubimci@progi-kalendar.iam.gserviceaccount.com";

	public boolean stvoriDogadaj(DogadajiForm dogadaj, int id) {
//		com.google.api.services.calendar.model.Events eventList;

// 			TokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectURI).execute();
// 			credential = flow.createAndStoreCredential(response, "userID"); //for Oauth2
		Preconditions.checkArgument(!Strings.isNullOrEmpty(APPLICATION_NAME),
				"applicationName cannot be null or empty!");
		try {
			TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			credential = new GoogleCredential.Builder().setTransport(TRANSPORT).setJsonFactory(JSON_FACTORY)
					.setServiceAccountId(SERVICE_ACCOUNT)
					.setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR))
					.setServiceAccountPrivateKeyFromP12File(keyFile)
					.build();
			credential.refreshToken();
			client = new com.google.api.services.calendar.Calendar.Builder(TRANSPORT, JSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME).build();
			System.out.println(client);
//			Events events = client.events();
//			eventList = events.list("primary").execute();
//			message = eventList.getItems().toString();

			Event event = new Event()
					.setId( convertId( id))
					.setLocation( dogadaj.getLokacija())
					.setDescription( dogadaj.getOpis());

			DateTime startDateTime = new DateTime( dogadaj.getStartDate().toInstant( ZoneOffset.ofHours(1)).toEpochMilli());
			EventDateTime start = new EventDateTime().setDateTime(startDateTime);
			event.setStart(start);

			DateTime endDateTime = new DateTime( dogadaj.getEndDate().toInstant( ZoneOffset.ofHours(1)).toEpochMilli());
			EventDateTime end = new EventDateTime().setDateTime(endDateTime);
			event.setEnd(end);

			event = client.events().insert(calendarId, event).execute();
			System.out.printf("Event created: %s\n", event.getHtmlLink());

			return true;
		} catch (IOException e) {
			System.out.println(e);
		} catch (GeneralSecurityException e) {
			System.out.println( e);
		}
		return false;
	}
	
	public List dajSveDogadaje() {

		Preconditions.checkArgument(!Strings.isNullOrEmpty(APPLICATION_NAME),
				"applicationName cannot be null or empty!");
		try {
			TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			credential = new GoogleCredential.Builder().setTransport(TRANSPORT).setJsonFactory(JSON_FACTORY)
					.setServiceAccountId(SERVICE_ACCOUNT)
					.setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR))
					.setServiceAccountPrivateKeyFromP12File(keyFile)
					.build();
			credential.refreshToken();
			client = new com.google.api.services.calendar.Calendar.Builder(TRANSPORT, JSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME).build();
			
			return client.events().list(calendarId);
		} catch (IOException e) {
			System.out.println(e);
		} catch (GeneralSecurityException e) {
			System.out.println( e);
		}
		return null;
	}
	
	private static String convertId( int id) {
		// mora biti izmedu 5 i 1024 charactera
		return String.format( "%05d", id);
	}
}
