package org.o7planning.sbsecurity.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.Profil_ljubimcaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.o7planning.sbsecurity.formbean.AppTvrtkaForm;
import org.o7planning.sbsecurity.formbean.TvrtkaUpdateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TvrtkaDAO {

	@Autowired
    private EntityManager entityManager;
	
	private static String tableName = "Tvrtka";
	
	public boolean createOsoba(AppTvrtkaForm form) {

		try {
			String sql = "insert into " + tableName + " (username, adresa, opis, naziv, kontakt) values(?, ?, NULL, ?, ?)";

			entityManager.createNativeQuery(sql)
					.setParameter(1, form.getUsername())
					.setParameter(2, form.getAddress())
					.setParameter(3, form.getName())
					.setParameter(4, form.getContact())
					.executeUpdate();
		} catch (NoResultException e) {
			return false;
		}

		return false;
	}
	
	public boolean postoji( String username) {
		String sql = "select e from " + TvrtkaENTITY.class.getName() + " e " //
                + " Where e.Korisnik.userName = :arg ";
        
    	Query query = entityManager.createQuery(sql, TvrtkaENTITY.class);
        query.setParameter("arg", username);
        
        return (boolean) (query.getResultList().size() > 0);
	}
	
	public TvrtkaENTITY pronadi( String username) {
		String sql = "select e from " + TvrtkaENTITY.class.getName() + " e " //
                + " Where e.Korisnik.userName = :arg ";
        
    	Query query = entityManager.createQuery(sql, TvrtkaENTITY.class);
        query.setParameter("arg", username);
        
        return (TvrtkaENTITY) query.getSingleResult();
	}
	
	public boolean azurirajPodatke( String username, TvrtkaUpdateForm tvrtkaUpdateForm) {
		try {
            String sql = "update " + TvrtkaENTITY.class.getName() +
            		" e SET e.adresa = :adresa, e.opis= :opis, e.naziv= :naziv, "+
            		" e.kontakt= :kontakt where e.Korisnik.userName = :username";
            
            entityManager.createQuery( sql)
            .setParameter("adresa", tvrtkaUpdateForm.getAdresa())
            .setParameter("opis", tvrtkaUpdateForm.getOpis())
            .setParameter("naziv", tvrtkaUpdateForm.getNaziv())
            .setParameter("kontakt", tvrtkaUpdateForm.getKontakt())
            .setParameter("username", username)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}
}
