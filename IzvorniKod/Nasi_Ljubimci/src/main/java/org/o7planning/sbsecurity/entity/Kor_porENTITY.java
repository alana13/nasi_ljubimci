package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "kor_por")
public class Kor_porENTITY {
	
	
	@Id
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "vrijeme", nullable = false)
	private java.sql.Date vrijeme;
	
	@Column(name = "sadrzaj", nullable = false)
	private String sadrzaj;
	
	@ManyToOne
	@JoinColumn(name = "username_1", nullable = false)
	private KorisnikENTITY korisnik1;
	
	@ManyToOne
	@JoinColumn(name = "username_2", nullable = false)
	private KorisnikENTITY korisnik2;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getVrijeme() {
		return vrijeme;
	}

	public void setVrijeme(Date vrijeme) {
		this.vrijeme = vrijeme;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	public KorisnikENTITY getKorisnik1() {
		return korisnik1;
	}

	public void setKorisnik1(KorisnikENTITY korisnik1) {
		this.korisnik1 = korisnik1;
	}

	public KorisnikENTITY getKorisnik2() {
		return korisnik2;
	}

	public void setKorisnik2(KorisnikENTITY korisnik2) {
		this.korisnik2 = korisnik2;
	}

	
}
