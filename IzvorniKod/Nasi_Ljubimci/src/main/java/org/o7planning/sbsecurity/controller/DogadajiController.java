package org.o7planning.sbsecurity.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.o7planning.sbsecurity.dao.DogadajDAO;
import org.o7planning.sbsecurity.dao.KomentarDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_dogDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.StatusDAO;
import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;
import org.o7planning.sbsecurity.formbean.DogadajiForm;
import org.o7planning.sbsecurity.service.DogadajiServiceImpl;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class DogadajiController {

	@Autowired
	private OsobaDAO osobaDAO;

	@Autowired
	private DogadajDAO dogadajDAO;

	@Autowired
	private Kor_kom_dogDAO kor_kom_dogDAO;

	@Autowired
	private KomentarDAO komentarDAO;

	@Autowired
	private DogadajiServiceImpl dogadajiService;

	@Autowired
	private StatusDAO statusDAO;

	// vraca stranicu za pregled dogadaja korisnika
	@RequestMapping(value = "/dogadaji", method = RequestMethod.GET)
	public String dajDogadaje(Model model, Principal principal) {
		User user = (User) ((Authentication) principal).getPrincipal();
		List<DogadajENTITY> dogadaji = dogadajDAO.dajDogadaje(user.getUsername());
		model.addAttribute("dogadaji", dogadaji);

		List<Kor_kom_dogENTITY> komentariDogađaja = kor_kom_dogDAO.dajSveKomentareNaDogađaje(dogadaji);
		Map<Integer, List<String>> komentariDog = WebUtils.komentarDogIzObjektaUString(komentariDogađaja);
		model.addAttribute("komentariDogadaja", komentariDog);
		model.addAttribute("korisnickoIme", user.getUsername());

		return "dogadaji";
	}

	// vraca stranicu za pregled dogadaja drugog korisnika
	@RequestMapping(value = "/pregledDogadaja", method = RequestMethod.GET)
	public String dajDogadajeKorisnika(Model model, Principal principal, @RequestParam("username") String username) {
		User user = (User) ((Authentication) principal).getPrincipal();
		List<DogadajENTITY> dogadaji;
		// ako je vlasnik i ako su prijatelji vidi sve
		if (user.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_VLASNIK"))
				&& statusDAO.jesuLiPrijatelji(username, user.getUsername())) {
			// ako je prijatelj s korisnikom vidi sve
			// dohvati sve dogadaje
			dogadaji = dogadajDAO.dajDogadaje(username);
		} else {
			// inače vidi samo javne
			// dohvati javne dogadaje
			dogadaji = dogadajDAO.dajJavneDogadaje(username);
		}
		System.out.println("dogadaji:: " + dogadaji);
		model.addAttribute("dogadaji", dogadaji);

		List<Kor_kom_dogENTITY> komentariDogađaja = kor_kom_dogDAO.dajSveKomentareNaDogađaje(dogadaji);
		Map<Integer, List<String>> komentariDog = WebUtils.komentarDogIzObjektaUString(komentariDogađaja);
		model.addAttribute("komentariDogadaja", komentariDog);
		model.addAttribute("korisnickoIme", user.getUsername());

		return "pregledDogadajaKorisnika";
	}

	// vraca formu za stvaranje dogadaja
	@RequestMapping(value = "/dogadajForm", method = RequestMethod.GET)
	public String dajFormuZaDogaje(Model model, Principal principal) {

		// ako je tvrtka onda u formi nemoj ponudit opciju za private
		boolean opcijaPrivate = true;
		User loginedUser = (User) ((Authentication) principal).getPrincipal();
		if (loginedUser.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_TVRTKA"))) {
			opcijaPrivate = false;
		}
		model.addAttribute("opcijaPrivate", opcijaPrivate);

		return "dogadajForm";
	}

	// stvara dogadaj
	@RequestMapping(value = "/stvoriDogadaj", method = RequestMethod.POST)
	public String stvoriDogadaj(Model model, Principal principal, @ModelAttribute("DogadajiForm") DogadajiForm form) {
		// ako je tvrtka postavi dogadaj na public
		User user = (User) ((Authentication) principal).getPrincipal();
		if (user.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_TVRTKA"))) {
			form.setPrivpubl(true);
		}

		dogadajDAO.stvoriDogadaj(user.getUsername(), form);
		int id = dogadajDAO.najveciId();
		dogadajiService.stvoriDogadaj(form, id);

		return "redirect:/preusmjeriProfil";
	}

	// upisuje komentar na danu objavu( id)
	@RequestMapping(value = "/komentirajDogadaj{id}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public String komentirajDogadaj(Model model, @PathVariable("id") Integer id,
			@RequestParam("komentar") String komentar, Principal principal) {
		if (komentar == null || komentar.length() < 1) {
			return null;
		}

		User user = (User) ((Authentication) principal).getPrincipal();
		String username = user.getUsername();
		model.addAttribute("korisnickoIme", user.getUsername());
		// TODO ovo se moze optimirat, kad se sprema komentar id se automatski generira
		// pa ga se mozda moze odma i vratit
		komentarDAO.spremiKomentar(komentar);
		int komentarId = komentarDAO.najveciId();
		kor_kom_dogDAO.spremiKomentar(username, id, komentarId);

		if (osobaDAO.postoji(username)) {
			return "redirect:/vlasnik/pregledDrugogProfila/" + username;
		}
		return null;
	}
}
