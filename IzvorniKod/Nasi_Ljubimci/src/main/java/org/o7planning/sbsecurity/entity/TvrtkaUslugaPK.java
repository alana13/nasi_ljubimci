package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class TvrtkaUslugaPK implements Serializable {

	private TvrtkaENTITY tvrtka;

	private UslugaENTITY usluga;

	public TvrtkaUslugaPK() {
		super();
	}

	public TvrtkaENTITY getTvrtka() {
		return tvrtka;
	}

	public void setTvrtka(TvrtkaENTITY tvrtka) {
		this.tvrtka = tvrtka;
	}

	public UslugaENTITY getUsluga() {
		return usluga;
	}

	public void setUsluga(UslugaENTITY usluga) {
		this.usluga = usluga;
	}

	@Override
	public int hashCode() {
		return Objects.hash(tvrtka, usluga);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TvrtkaUslugaPK other = (TvrtkaUslugaPK) obj;
		return Objects.equals(tvrtka, other.tvrtka) && Objects.equals(usluga, other.usluga);
	}
}
