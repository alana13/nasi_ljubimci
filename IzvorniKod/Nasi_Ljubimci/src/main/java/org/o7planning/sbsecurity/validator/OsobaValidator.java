package org.o7planning.sbsecurity.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.entity.KorisnikENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class OsobaValidator implements Validator {

	private EmailValidator emailValidator = EmailValidator.getInstance();

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private OsobaDAO osobaDAO;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AppOsobaForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AppOsobaForm appOsobaForm = (AppOsobaForm) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.appForm.username");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.appOsobaForm.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.appOsobaForm.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.appOsobaForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.appForm.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty.appForm.confirmPassword");

		// check email
		if (!this.emailValidator.isValid(appOsobaForm.getEmail())) {
			// Invalid email.
			errors.rejectValue("email", "Pattern.appOsobaForm.email");
		}
		
		OsobaENTITY korisnik = osobaDAO.findOsobaByEmail(appOsobaForm.getEmail());
		if (korisnik != null) {
			// Email has been used by another account.
			errors.rejectValue("email", "Duplicate.appOsobaForm.email");
		}

		// check user name
		if (!errors.hasFieldErrors("username")) {
			KorisnikENTITY korisnik2 = korisnikDAO.findAppUserByName(appOsobaForm.getUsername());
			if (korisnik2 != null) {
				// Username is not available.
				errors.rejectValue("username", "Duplicate.appForm.username");
			}
		}

		// check password
		if (!errors.hasErrors()) {
			if (!appOsobaForm.getConfirmPassword().equals(appOsobaForm.getPassword())) {
				errors.rejectValue("confirmPassword", "Match.appForm.confirmPassword");
			}
		}
	}
}
