package org.o7planning.sbsecurity.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(KorisnikPK.class)
@Table(name = "Korisnik")
public class KorisnikENTITY{

    @Id
    @Column(name = "username", length = 36, nullable = false)
    private String userName;
    
    @Column(name = "password", length = 128, nullable = false)
    private String password;

    @Column(name = "enabled", nullable = false)
    private Integer enabled;
    
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	@Override
	public int hashCode() {
		return Objects.hash(enabled, password, userName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KorisnikENTITY other = (KorisnikENTITY) obj;
		return Objects.equals(enabled, other.enabled)
				&& Objects.equals(password, other.password) && Objects.equals(userName, other.userName);
	}
	
}
