package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@IdClass(TvrtkaPK.class)
@Table(name = "Tvrtka")
public class TvrtkaENTITY {

	@Id
	@OneToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;

	@Column(name = "adresa", length = 50, nullable = false)
	private String adresa;

	@Column(name = "opis", nullable = true)
	private String opis;

	@Column(name = "naziv", length = 36, nullable = false)
	private String naziv;

	@Column(name = "kontakt", length = 36, nullable = false)
	private String kontakt;

	@Column(name = "datum_registracije", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date datumRegistracije;

	public Date getDatumRegistracije() {
		return datumRegistracije;
	}

	public void setDatumRegistracije(Date datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		this.Korisnik = korisnik;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getKontakt() {
		return kontakt;
	}

	public void setKontakt(String kontakt) {
		this.kontakt = kontakt;
	}

}