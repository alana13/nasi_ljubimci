package org.o7planning.sbsecurity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass( TvrtkaUslugaPK.class)
@Table(name = "tvrtka_usluga")
public class TvrtkaUslugaENTITY {
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	private TvrtkaENTITY tvrtka;
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uslugaid", nullable = false)
	private UslugaENTITY usluga;
	
	@Column(name = "opis", nullable = true)
	private String opis;

	public TvrtkaENTITY getTvrka() {
		return tvrtka;
	}

	public void setTvrka(TvrtkaENTITY tvrtka) {
		this.tvrtka = tvrtka;
	}

	public UslugaENTITY getUsluga() {
		return usluga;
	}

	public void setUsluga(UslugaENTITY usluga) {
		this.usluga = usluga;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	
	
}
