package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class Tag_ljubimcaPK implements Serializable{
	
	private Profil_ljubimcaENTITY profilLjubimca;
	
	private ObjavaENTITY objava;

	

	public Tag_ljubimcaPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Profil_ljubimcaENTITY getProfiLjubimca() {
		return profilLjubimca;
	}

	public void setProfiLjubimca(Profil_ljubimcaENTITY profiLjubimca) {
		this.profilLjubimca = profiLjubimca;
	}

	public ObjavaENTITY getObjava() {
		return objava;
	}

	public void setObjava(ObjavaENTITY objava) {
		this.objava = objava;
	}

	@Override
	public int hashCode() {
		return Objects.hash(objava, profilLjubimca);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag_ljubimcaPK other = (Tag_ljubimcaPK) obj;
		return Objects.equals(objava, other.objava) && Objects.equals(profilLjubimca, other.profilLjubimca);
	}

}
