package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class OsobaPK implements Serializable {
	
	private KorisnikENTITY korisnik;

	public OsobaPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikENTITY getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public int hashCode() {
		return Objects.hash(korisnik);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OsobaPK other = (OsobaPK) obj;
		return Objects.equals(korisnik, other.korisnik);
	}
	
	
	
}
