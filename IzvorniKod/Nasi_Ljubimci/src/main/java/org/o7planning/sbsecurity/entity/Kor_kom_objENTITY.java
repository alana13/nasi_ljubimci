package org.o7planning.sbsecurity.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@IdClass(Kor_kom_objPK.class)
@Table(name = "kor_kom_obj")
public class Kor_kom_objENTITY {
	
	@Id
	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;
	
	@Id
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idObj", nullable = false)
	private ObjavaENTITY Objava;
	
	@Id
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idKom", nullable = false)
	private KomentarENTITY Komentar;

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

	public ObjavaENTITY getObjava() {
		return Objava;
	}

	public void setObjava(ObjavaENTITY objava) {
		Objava = objava;
	}

	public KomentarENTITY getKomentar() {
		return Komentar;
	}

	public void setKomentar(KomentarENTITY komentar) {
		Komentar = komentar;
	}
	
	
}
