package org.o7planning.sbsecurity.service;

import java.util.ArrayList;
import java.util.List;

import org.o7planning.sbsecurity.dao.BansDAO;
import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.UserRoleDAO;
import org.o7planning.sbsecurity.entity.KorisnikENTITY;
import org.o7planning.sbsecurity.models.Administrator;
import org.o7planning.sbsecurity.models.Tvrtka;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Lazy
    @Autowired
    private KorisnikDAO korisnikDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    private BansDAO bansDAO;
    
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        KorisnikENTITY korisnik = korisnikDAO.findAppUserByName(userName);
        
        if (korisnik == null) {
            System.out.println("User not found! " + userName);
            throw new UsernameNotFoundException("User " + userName + " was not found in the database");
        }

        boolean enabled = !bansDAO.isBanned( korisnik.getUserName());
        System.out.println("Found User: " + korisnik);
        // [ROLE_ADMIN, ROLE_VLASNIK, ROLE_TVRTKA]
        List<String> roleNames = userRoleDAO.getRoleNames( korisnik.getUserName());
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>(); 
        String roleName = "";
        if (roleNames != null) {
        	// korisnik moze imati samo jedan role
        	roleName = roleNames.get(0);
        	GrantedAuthority authority = new SimpleGrantedAuthority(roleName);
        	grantList.add(authority);
        }
        
        UserDetails userDetails;
        if( roleName.equals( "ROLE_TVRTKA")) {
        	userDetails = (UserDetails) new Tvrtka( korisnik.getUserName(), korisnik.getPassword(), grantList, enabled);
        }else if( roleName.equals( "ROLE_VLASNIK")) {
        	userDetails = (UserDetails) new Vlasnik( korisnik.getUserName(), korisnik.getPassword(), grantList, enabled);
        }else{
        	// moze biti samo role_admin
        	userDetails = (UserDetails) new Administrator(korisnik.getUserName(), korisnik.getPassword(), grantList, enabled);
        }

        return userDetails;
    }

}