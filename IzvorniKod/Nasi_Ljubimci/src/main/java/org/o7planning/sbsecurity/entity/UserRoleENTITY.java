package org.o7planning.sbsecurity.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(UserRolePK.class)
@Table(name = "userrole")
public class UserRoleENTITY {

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY korisnik;

	@Id
	@ManyToOne
	@JoinColumn(name = "roleid", nullable = false)
	private AppRoleENTITY approle;

	public KorisnikENTITY getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		this.korisnik = korisnik;
	}

	public AppRoleENTITY getApprole() {
		return approle;
	}

	public void setApprole(AppRoleENTITY approle) {
		this.approle = approle;
	}

}