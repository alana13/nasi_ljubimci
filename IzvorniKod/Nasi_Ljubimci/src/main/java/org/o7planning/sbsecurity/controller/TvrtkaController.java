package org.o7planning.sbsecurity.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.o7planning.sbsecurity.dao.DogadajDAO;
import org.o7planning.sbsecurity.dao.KomentarDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_dogDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_objDAO;
import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.PorukaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaUslugaDAO;
import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaUslugaENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.o7planning.sbsecurity.formbean.AppTvrtkaForm;
import org.o7planning.sbsecurity.formbean.DogadajiForm;
import org.o7planning.sbsecurity.formbean.LjubimacForm;
import org.o7planning.sbsecurity.formbean.TvrtkaUpdateForm;
import org.o7planning.sbsecurity.models.Tvrtka;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.o7planning.sbsecurity.utils.TvrtkaUtils;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/tvrtka")
public class TvrtkaController {

	@Autowired
	private ObjavaDAO objavaDAO;

	@Autowired
	private TvrtkaDAO tvrtkaDAO;

	@Autowired
	private Kor_kom_objDAO kor_kom_objDAO;

	@Autowired
	private TvrtkaUslugaDAO tvrtkaUslugaDAO;

	@Autowired
	private OsobaDAO osobaDAO;
	
	@Autowired
	private DogadajDAO dogadajDAO;
	
	@Autowired
	private Kor_kom_dogDAO kor_kom_dogDAO;

	// glavna stranica: profil tvrtke
	@RequestMapping(value = "/profil", method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {
		Tvrtka tvrtka = (Tvrtka) ((Authentication) principal).getPrincipal();

		// dohvati informacije o tvrtki
		// podaci
		TvrtkaENTITY podaci = tvrtkaDAO.pronadi(tvrtka.getUsername());
		// usluge
		List<TvrtkaUslugaENTITY> usluge = tvrtkaUslugaDAO.dajUsluge(tvrtka.getUsername());
		Map<String, String> informacije = TvrtkaUtils.mapirajUINformacije(podaci, usluge);
		model.addAttribute("informacije", informacije);

		// dohvati sve objave
		List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(tvrtka.getUsername());
		model.addAttribute("sveObjave", sveObjave);

		// dohavti komentare na objave
		List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
		Map<Integer, List<Kor_kom_objENTITY>> komentari = TvrtkaUtils.komentarIzObjektaUString(komentariObjekti);
		model.addAttribute("komentari", komentari);
		return "tvrtkaProfil";
	}

	// vraca formu za objave
	@RequestMapping(value = "/objava", method = RequestMethod.GET)
	public String objavaGet(Model model) {
		return "tvrtkaObjava";
	}

	// prihvaca ispunjenu formu za objave
	@RequestMapping(value = "/objava", method = RequestMethod.POST)
	public String objavaPOST(Model model, @RequestParam("kratkaPoruka") String kratkaPoruka,
			@RequestParam("file") MultipartFile file, Principal principal) {
		Tvrtka tvrtka = (Tvrtka) ((Authentication) principal).getPrincipal();

		if (file == null) {
			if (kratkaPoruka == null || kratkaPoruka.trim().length() < 1) {
				return "redirect:/tvrtka/profil";
			}
		}

		if (!file.isEmpty()) {
			byte[] data = null;
			String contentType = "";
			try {
				data = file.getBytes();
				contentType = file.getContentType();
			} catch (IOException e) {
				e.printStackTrace();
			}
			objavaDAO.spremiObjavuPorukaSlikaVideo(tvrtka.getUsername(), kratkaPoruka, data, contentType, true);
		} else {
			objavaDAO.spremiObjavuPoruka(tvrtka.getUsername(), kratkaPoruka, true);
		}

		return "redirect:/tvrtka/profil";
	}
	
	@RequestMapping(value = "/pregledDrugogProfila", method = RequestMethod.POST)
	public String pretrazivanje(Model model, @RequestParam("username") String username, Principal principal,
			RedirectAttributes redirAttr) {
		return "redirect:/tvrtka/pregledDrugogProfila/" + username;

	}
		
	@RequestMapping(value = "/pregledDrugogProfila/{username}", method = RequestMethod.GET)
	public String pregledProfila(Model model, Principal principal, @PathVariable("username") String username, 
			RedirectAttributes redirAttr) {
		if (username == null || username.trim().length() < 1) {
			redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
			return "redirect:/tvrtka/profil";
		}

		// ako je uneseno ime tvrtka ( pazi na samog sebe)
		User user = (User) ((Authentication) principal).getPrincipal();
		if (username.equals(user.getUsername())) {
			return "redirect:/tvrtka/profil";
		}

		if(tvrtkaDAO.postoji(username)) {

			model.addAttribute("username", username);

			// dohvati informacije o tvrtki
			// podaci
			TvrtkaENTITY podaci = tvrtkaDAO.pronadi(username);
			// usluge
			List<TvrtkaUslugaENTITY> usluge = tvrtkaUslugaDAO.dajUsluge(username);
			Map<String, String> informacije = TvrtkaUtils.mapirajUINformacije(podaci, usluge);
			model.addAttribute("informacije", informacije);

			// dohavati sve objave od tvtke
			List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(username);
			model.addAttribute("sveObjave", sveObjave);

			// dohvati sve komentare
			// dohavti komentare na objave
			List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
			Map<Integer, List<Kor_kom_objENTITY>> komentari = TvrtkaUtils.komentarIzObjektaUString(komentariObjekti);
			model.addAttribute("komentari", komentari);
			
			return "pregledTvrtke";
		} else if(osobaDAO.postoji(username)) {
			// ako je uneseno ime korisnik
			model.addAttribute("username", username);
			
			// dohvati podatke
			OsobaENTITY podaci = osobaDAO.findOsobaByName( username);
			model.addAttribute("podaci", podaci);
			
			return "tvrtkaPregledavaVlasnika";
		} else {
			// admin
		}

		// ne postoji taj korisnik
		redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
		return "redirect:/tvrtka/profil";
	}

	// vraca formu za promjenu podataka
	@RequestMapping(value = "/urediPodatke", method = RequestMethod.GET)
	public String urediPodatke(Model model, Principal principal) {
		Tvrtka tvrtka = (Tvrtka) ((Authentication) principal).getPrincipal();
		// podaci
		TvrtkaENTITY podaci = tvrtkaDAO.pronadi(tvrtka.getUsername());
		// usluge
		List<TvrtkaUslugaENTITY> usluge = tvrtkaUslugaDAO.dajUsluge(tvrtka.getUsername());
		Map<String, String> informacije = TvrtkaUtils.mapirajUINformacije(podaci, usluge);

		// popuni form objekt
		TvrtkaUpdateForm form = new TvrtkaUpdateForm();
		TvrtkaUtils.popuniTvrtkaUpdateForm(informacije, form);
		model.addAttribute("tvrtkaUpdateForm", form);
		model.addAttribute("naziv",podaci.getNaziv());
		return "urediPodatkeTvrtka";
	}

	// url za azuriranje podataka
	// TODO napravi validaciju podataka
	@RequestMapping(value = "/azurirajPodatke", method = RequestMethod.POST)
	public String saveRegisterTvrtka(Model model, //
			@ModelAttribute("tvrtkaUpdateForm") TvrtkaUpdateForm tvrtkaUpdateForm, //
			BindingResult result, Principal principal) {
		Tvrtka tvrtka = (Tvrtka) ((Authentication) principal).getPrincipal();
		// Validate result
		if (result.hasErrors()) {
			return "registerTvrtkaPage";
		}

		tvrtkaDAO.azurirajPodatke(tvrtka.getUsername(), tvrtkaUpdateForm);
		tvrtkaUslugaDAO.pobrisiUsluge(tvrtka.getUsername());
		tvrtkaUslugaDAO.zapisiUsluge(tvrtka.getUsername(), tvrtkaUpdateForm);

		return "redirect:/tvrtka/profil";
	}
}
