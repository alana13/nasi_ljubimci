package org.o7planning.sbsecurity.formbean;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

public class DogadajiForm {

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime startDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime endDate;

	private String lokacija;
	private String opis;
	private Boolean privpubl;

	public DogadajiForm() {
	}

	public DogadajiForm(LocalDateTime startDate, LocalDateTime endDate, String lokacija, String opis,
			boolean privpubl) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.lokacija = lokacija;
		this.opis = opis;
		this.privpubl = privpubl;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getLokacija() {
		return lokacija;
	}

	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Boolean isPrivpubl() {
		return privpubl;
	}

	public void setPrivpubl(Boolean privpubl) {
		this.privpubl = privpubl;
	}

	@Override
	public String toString() {
		return "DogadajiForm [startDate=" + startDate + ", endDate=" + endDate + ", lokacija=" + lokacija + ", opis="
				+ opis + ", privpubl=" + privpubl + "]";
	};
	
	
}
