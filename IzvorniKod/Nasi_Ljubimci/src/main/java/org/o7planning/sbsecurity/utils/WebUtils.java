package org.o7planning.sbsecurity.utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import org.o7planning.sbsecurity.entity.KomentarENTITY;

import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.PorukaENTITY;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class WebUtils {

    public static String toString(User user) {
        StringBuilder sb = new StringBuilder();

        sb.append("UserName:").append(user.getUsername());

        Collection<GrantedAuthority> authorities = user.getAuthorities();
        if (authorities != null && !authorities.isEmpty()) {
            sb.append(" (");
            boolean first = true;
            for (GrantedAuthority a : authorities) {
                if (first) {
                    sb.append(a.getAuthority());
                    first = false;
                } else {
                    sb.append(", ").append(a.getAuthority());
                }
            }
            sb.append(")");
        }
        return sb.toString();
    }
    
    public static Map<Integer, List<Kor_kom_objENTITY>> komentarIzObjektaUString( List<Kor_kom_objENTITY> komentari){

    	return komentari.stream()
    			.collect(Collectors.groupingBy(
    					e -> e.getObjava().getId(), 
    					Collectors.mapping( e -> {return e;}, Collectors.toList())));
    }
    
    public static Map<Integer, List<String>> komentarDogIzObjektaUString(List<Kor_kom_dogENTITY> komentari){
    	return komentari.stream()
    			.collect(Collectors.groupingBy(
    					e -> e.getDogadaj().getId(), 
    					Collectors.mapping( e -> {String username = e.getKorisnik().getUserName();
						  String komentar = e.getKomentar().getSadrzaj();
						  return username + " : " + komentar;}, Collectors.toList())));
    }

	public static List<String> pretvoriPoruke(List<PorukaENTITY> poruke) {
		List<String> result = new LinkedList<>();
		
		if( poruke != null) {
			for( PorukaENTITY p : poruke) {
				result.add( p.getDatum() + " " + p.getPosiljatelj().getUserName() + ": " + p.getSadrzaj());
			}
		}
		
		return result;
	}
    
}