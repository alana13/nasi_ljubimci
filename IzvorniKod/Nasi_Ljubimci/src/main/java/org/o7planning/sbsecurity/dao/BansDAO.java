package org.o7planning.sbsecurity.dao;

import java.time.Instant;
import java.time.LocalDateTime;
import java.sql.Timestamp;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class BansDAO {

	@Autowired
	private EntityManager entityManager;

	private static String tableName = "bans";

	public boolean banUser(String username, LocalDateTime banUntil) {
		String sql = " insert into " + tableName + " (username, datum_kraj) " + " values (?, ?)";

		entityManager.createNativeQuery(sql)
			.setParameter(1, username)
			.setParameter(2, banUntil)
			.executeUpdate();

		return true;
	}
	
	public boolean isBanned( String username) {
		String sql = " select max( datum_kraj) from " + tableName + " where username = ?";
		
		Timestamp time = (Timestamp) entityManager.createNativeQuery(sql)
				.setParameter(1, username).getSingleResult();
		
		return time != null && time.after( Timestamp.from( Instant.now()));
	}
}
