package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Profil_ljubimca")
public class Profil_ljubimcaENTITY {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",nullable = false, columnDefinition = "serial")
	private int id;

	@Column(name = "vrsta", length = 50, nullable = false)
	private String vrsta;

	@Column(name = "ime", length = 50, nullable = false)
	private String ime;

	@Column(name = "datum_rodenja",nullable = false)
	private Date datum_rodenja;

	@Column(name = "spol", length = 20, nullable = false)
	private String spol;

	@Column(name = "profilna_fotografija",nullable = true)
	private String profilna_fotografija;
	
	@Column(name = "data", nullable = true)
	private byte[] data;
	
	@Column(name = "opis",nullable = false)
	private String opis;
	
	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private OsobaENTITY Osoba;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Date getDatum_rodenja() {
		return datum_rodenja;
	}

	public void setDatum_rodenja(Date datum_rodenja) {
		this.datum_rodenja = datum_rodenja;
	}

	public String getSpol() {
		return spol;
	}

	public void setSpol(String spol) {
		this.spol = spol;
	}


	public String getProfilna_fotografija() {
		return profilna_fotografija;
	}

	public void setProfilna_fotografija(String profilna_fotografija) {
		this.profilna_fotografija = profilna_fotografija;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public OsobaENTITY getOsoba() {
		return Osoba;
	}

	public void setOsoba(OsobaENTITY osoba) {
		Osoba = osoba;
	}
	
	
	

}
