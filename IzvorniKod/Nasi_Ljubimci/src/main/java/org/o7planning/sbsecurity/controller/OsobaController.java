package org.o7planning.sbsecurity.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.o7planning.sbsecurity.dao.DogadajDAO;
import org.o7planning.sbsecurity.dao.KomentarDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_dogDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_objDAO;
import org.o7planning.sbsecurity.dao.LjubimacDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.StatusDAO;
import org.o7planning.sbsecurity.dao.TvrtkaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaUslugaDAO;
import org.o7planning.sbsecurity.entity.DogadajENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_dogENTITY;
import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.entity.Profil_ljubimcaENTITY;
import org.o7planning.sbsecurity.entity.Tag_ljubimcaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaUslugaENTITY;
import org.o7planning.sbsecurity.formbean.LjubimacForm;
import org.o7planning.sbsecurity.formbean.TvrtkaUpdateForm;
import org.o7planning.sbsecurity.formbean.VlasnikUpdateForm;
import org.o7planning.sbsecurity.models.Tvrtka;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.o7planning.sbsecurity.utils.OsobaUtils;
import org.o7planning.sbsecurity.utils.TvrtkaUtils;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/vlasnik")
public class OsobaController {

	@Autowired
	private OsobaDAO osobaDAO;

	@Autowired
	private ObjavaDAO objavaDAO;

	@Autowired
	private LjubimacDAO ljubimacDAO;

	@Autowired
	private Kor_kom_objDAO kor_kom_objDAO;

	@Autowired
	private TvrtkaDAO tvrtkaDAO;

	@Autowired
	private StatusDAO statusDAO;

	@Autowired
	private TvrtkaUslugaDAO tvrtkaUslugaDAO;

	@RequestMapping(value = { "/profil", "/" }, method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();

		// dohvati podatke
		OsobaENTITY podaci = osobaDAO.findOsobaByName(osoba.getUsername());
		model.addAttribute("podaci", podaci);

		// dohvati sve objave
		List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(osoba.getUsername());
		model.addAttribute("sveObjave", sveObjave);

		// dohavti komentare na objave
		List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
		Map<Integer, List<Kor_kom_objENTITY>> komentari = WebUtils.komentarIzObjektaUString(komentariObjekti);
		model.addAttribute("komentari", komentari);

		List<Profil_ljubimcaENTITY> ljubimci = ljubimacDAO.listAllLjubimci(osoba.getUsername());
		model.addAttribute("sviLjubimci", ljubimci);

		model.addAttribute("sviZahtjevi", statusDAO.sviZahtjevi(osoba.getUsername()));
		model.addAttribute("osoba", osoba);

		return "vlasnikProfil";
	}

	@RequestMapping(value = "/noviljubimac", method = RequestMethod.GET)
	public String kreiranje_ljubimca(Model model, Principal principal) {
		return "noviLjubimac";
	}

	@RequestMapping(value = "/noviljubimac", method = RequestMethod.POST)
	public String kreiranje_ljubimcaform(Model model, Principal principal,
			@ModelAttribute("LjubimacForm") LjubimacForm ljubimacForm,
			@RequestParam("profilna_fotografija") MultipartFile file) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		if (!file.isEmpty()) {
			byte[] data = null;
			String contentType = null;
			try {
				data = file.getBytes();
				contentType = file.getContentType();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (ljubimacDAO.createLjubimac(ljubimacForm, data, contentType, osoba.getUsername()) == true) {
				System.out.println("Ljubimac je kreiran");
			} else
				System.out.println("Ljubimac nije kreiran");
		}
		return "redirect:/vlasnik/profil";
	}

	@RequestMapping(value = "/objava", method = RequestMethod.GET)
	public String objavaGet(Model model) {
		return "vlasnikObjava";
	}

	@RequestMapping(value = "/objava", method = RequestMethod.POST)
	public String objavaPOST(Model model, @RequestParam("kratkaPoruka") String kratkaPoruka,
			@RequestParam("file") MultipartFile file, Principal principal) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		Tag_ljubimcaENTITY tag;

		if (file == null) {
			if (kratkaPoruka == null || kratkaPoruka.trim().length() < 1) {
				return "redirect:/vlasnik/profil";
			}
		}

		if (!file.isEmpty()) {
			byte[] data = null;
			String contentType = "";
			try {
				data = file.getBytes();
				contentType = file.getContentType();
			} catch (IOException e) {
				e.printStackTrace();
			}
			objavaDAO.spremiObjavuPorukaSlikaVideo(osoba.getUsername(), kratkaPoruka, data, contentType, true);
		} else {
			objavaDAO.spremiObjavuPoruka(osoba.getUsername(), kratkaPoruka, true);
		}

		return "redirect:/vlasnik/profil";
	}

	// vraca formu za promjenu podataka
	@RequestMapping(value = "/urediPodatke", method = RequestMethod.GET)
	public String urediPodatke(Model model, Principal principal) {
		Vlasnik vlasnik = (Vlasnik) ((Authentication) principal).getPrincipal();
		// podaci
		OsobaENTITY podaci = osobaDAO.findOsobaByName(vlasnik.getUsername());
		VlasnikUpdateForm form = new VlasnikUpdateForm();
		OsobaUtils.popuniTvrtkaUpdateForm(podaci, form);
		model.addAttribute("vlasnikUpdateForm", form);
		model.addAttribute("ime", podaci.getFirstname());
		model.addAttribute("prezime", podaci.getUserLastname());

		return "urediPodatkeVlasnik";
	}

	// dohvati profilnu
	@RequestMapping(value = "/profilnaFotografija", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageAsByteArray(Principal principal) throws IOException {
		Vlasnik vlasnik = (Vlasnik) ((Authentication) principal).getPrincipal();
		// podaci
		OsobaENTITY podaci = osobaDAO.findOsobaByName(vlasnik.getUsername());

		return ResponseEntity.ok().body(podaci.getProfilnaFotografija());
	}

	// url za azuriranje podataka
	// TODO napravi validaciju podataka
	@RequestMapping(value = "/azurirajPodatke", method = RequestMethod.POST)
	public String azurirajPodatke(Model model, //
			@ModelAttribute("vlasnikUpdateForm") VlasnikUpdateForm vlasnikUpdateForm,
			BindingResult result, Principal principal) {
		Vlasnik vlasnik = (Vlasnik) ((Authentication) principal).getPrincipal();

		try {
			osobaDAO.azurirajPodatke(vlasnik.getUsername(), vlasnikUpdateForm);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/vlasnik/profil";
	}

	// salje formu samo s usernameom kojeg zeli pregledati i redirecta na get
	// stranicu
	@RequestMapping(value = "/pregledDrugogProfila", method = RequestMethod.POST)
	public String pretrazivanje(Model model, @RequestParam("username") String username, Principal principal,
			RedirectAttributes redirAttr) {
		return "redirect:/vlasnik/pregledDrugogProfila/" + username;

	}

	// vraca sve podatke potrebne za pregled drugog profila
	@RequestMapping(value = "/pregledDrugogProfila/{username}", method = RequestMethod.GET)
	public String pregledProfila(Model model, Principal principal, @PathVariable("username") String username,
			RedirectAttributes redirAttr) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		if (username == null || username.trim().length() < 1) {
			redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
			return "redirect:/vlasnik/profil";
		}

		// ako je uneseno ime isto ( pazi na samog sebe)
		User user = (User) ((Authentication) principal).getPrincipal();
		if (username.equals(user.getUsername())) {
			return "redirect:/vlasnik/profil";
		}
		// roleId = userRoleDAO.getRoleId(username);
		if (tvrtkaDAO.postoji(username)) {
			model.addAttribute("username", username);

			// dohvati informacije o tvrtki
			// podaci
			TvrtkaENTITY podaci = tvrtkaDAO.pronadi(username);
			// usluge
			List<TvrtkaUslugaENTITY> usluge = tvrtkaUslugaDAO.dajUsluge(username);
			Map<String, String> informacije = TvrtkaUtils.mapirajUINformacije(podaci, usluge);
			model.addAttribute("informacije", informacije);

			// dohavati sve objave od tvtke
			List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(username);
			model.addAttribute("sveObjave", sveObjave);

			// dohvati sve komentare
			// dohavti komentare na objave
			List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
			Map<Integer, List<Kor_kom_objENTITY>> komentari = TvrtkaUtils.komentarIzObjektaUString(komentariObjekti);
			model.addAttribute("komentari", komentari);

			return "pregledTvrtke";
		} else if (osobaDAO.postoji(username)) {
			boolean check = statusDAO.jesuLiPrijatelji(osoba.getUsername(), username);
			if (statusDAO.korisnikJeBlokiran(osoba.getUsername(), username)) {
				model.addAttribute("username", username);
				return "pregledVlasnikaBlokiran";
			}
			if (statusDAO.korisnikVasJeBlokirao(osoba.getUsername(), username)) {
				model.addAttribute("username", username);
				return "pregledVlasnikaBlokiranOdKorisnika";
			}
			System.out.println(check);
			if (check) {
				// for(KorisnikENTITY kor: )
				model.addAttribute("username", username);

				// dohavati sve objave od tvtke
				List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(username);
				model.addAttribute("sveObjave", sveObjave);
				//
				// dohvati sve komentare
				// dohavti komentare na objave
				List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
				Map<Integer, List<Kor_kom_objENTITY>> komentari = WebUtils.komentarIzObjektaUString(komentariObjekti);
				model.addAttribute("komentari", komentari);
				OsobaENTITY podaci = osobaDAO.findOsobaByName(username);
				model.addAttribute("podaci", podaci);

				List<Profil_ljubimcaENTITY> ljubimci = ljubimacDAO.listAllLjubimci( username);
				model.addAttribute("sviLjubimci", ljubimci);
				return "pregledVlasnika";
			} else {
				model.addAttribute("username", username);

				model.addAttribute("zahtjevVecDobiven", statusDAO.jeliZahtjevPoslan(username, osoba.getUsername()));
				model.addAttribute("zahtjevVecPoslan", statusDAO.jeliZahtjevPoslan(osoba.getUsername(), username));

				return "pregledVlasnika2";
			}
		}
		// ne postoji taj korisnik
		redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
		return "redirect:/vlasnik/profil";
	}

	// salje zahtjev za prijateljstvo, ako ne uspije, salje ga na svoj profil, ako
	// uspije poslati onda ga salje na profil koji gleda
	@RequestMapping(value = "/posaljiZahtjev{username}", method = RequestMethod.GET)
	public String slanjeZahtjeva(Model model, Principal principal, @PathVariable("username") String username) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		if (statusDAO.PosaljiZahtjev(osoba.getUsername(), username) == false) {
			return "redirect:/vlasnik/profil";
		}
		return "redirect:/vlasnik/pregledDrugogProfila/" + username;
	}

	@RequestMapping(value = "/prihvatiZahtjev{username}", method = RequestMethod.GET)
	public String prihvatiZahtjev(Model model, Principal principal, @PathVariable("username") String username) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		System.out.println(statusDAO.DodajPrijatelja(osoba.getUsername(), username));
		return "redirect:/vlasnik/profil";
	}

	@RequestMapping(value = "/odbijZahtjev{username}", method = RequestMethod.GET)
	public String odbijZahtjev(Model model, Principal principal, @PathVariable("username") String username) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		System.out.println(statusDAO.MakniPrijatelja(osoba.getUsername(), username));
		return "redirect:/vlasnik/profil";
	}

	@RequestMapping(value = "/blokirajKorisnika{username}", method = RequestMethod.GET)
	public String blokirajKorisnika(Model model, Principal principal, @PathVariable("username") String username) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		System.out.println(statusDAO.BlokirajKorisnika(osoba.getUsername(), username));
		return "redirect:/vlasnik/pregledDrugogProfila/" + username;
	}

	@RequestMapping(value = "/odblokirajKorisnika{username}", method = RequestMethod.GET)
	public String odblokirajKorisnika(Model model, Principal principal, @PathVariable("username") String username) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		System.out.println(statusDAO.OdBlokirajKorisnika(osoba.getUsername(), username));
		return "redirect:/vlasnik/pregledDrugogProfila/" + username;
	}

	@RequestMapping(value = "/obrisiLjubimca/{id}")
	public String obrisiLjubimca(Model model, Principal principal, @PathVariable("id") int id) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		ljubimacDAO.ObrisiLjubimca(id);
		return "redirect:/vlasnik/profil";
	}
	
	@RequestMapping( value= "prijatelji", method = RequestMethod.GET)
	public String prijatelji( Model model, Principal principal) {
		Vlasnik osoba = (Vlasnik) ((Authentication) principal).getPrincipal();
		List<String> usernamePrijatelja = statusDAO.sviPrijatelji( osoba.getUsername());
		model.addAttribute( "prijatelji", usernamePrijatelja);
		return "pregledPrijatelja";
	}
}
