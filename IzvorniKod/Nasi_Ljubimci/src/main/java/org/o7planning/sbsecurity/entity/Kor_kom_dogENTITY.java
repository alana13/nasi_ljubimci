package org.o7planning.sbsecurity.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@IdClass(Kor_kom_dogPK.class)
@Table(name = "kor_kom_dog")
public class Kor_kom_dogENTITY {
	
	@Id
	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "idDog", nullable = false)
	private DogadajENTITY Dogadaj;
	
	@Id
	@OneToOne
	@JoinColumn(name = "idKom", nullable = false)
	private KomentarENTITY Komentar;

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

	public DogadajENTITY getDogadaj() {
		return Dogadaj;
	}

	public void setDogadaj(DogadajENTITY dogadaj) {
		Dogadaj = dogadaj;
	}

	public KomentarENTITY getKomentar() {
		return Komentar;
	}

	public void setKomentar(KomentarENTITY komentar) {
		Komentar = komentar;
	}
	
	

}
