package org.o7planning.sbsecurity.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Bans")
public class BansENTITY {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "serial")
	private int id;

	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;

	@Column(name = "datum_kraj", nullable = false, columnDefinition = "TIMESTAMP")
	private LocalDateTime endDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	
}
