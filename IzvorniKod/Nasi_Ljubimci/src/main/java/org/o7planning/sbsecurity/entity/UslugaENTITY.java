package org.o7planning.sbsecurity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Usluga")
public class UslugaENTITY {

	@Id
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "vrsta_usluge", nullable = false)
	private String vrsta_usluge;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVrsta_usluge() {
		return vrsta_usluge;
	}

	public void setVrsta_usluge(String vrsta_usluge) {
		this.vrsta_usluge = vrsta_usluge;
	}
}
