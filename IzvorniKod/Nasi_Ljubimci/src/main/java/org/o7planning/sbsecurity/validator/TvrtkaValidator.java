package org.o7planning.sbsecurity.validator;

import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.TvrtkaDAO;
import org.o7planning.sbsecurity.entity.KorisnikENTITY;
import org.o7planning.sbsecurity.formbean.AppTvrtkaForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TvrtkaValidator implements Validator {

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private TvrtkaDAO tvrtkaDAO;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AppTvrtkaForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AppTvrtkaForm appTvrtkaForm = (AppTvrtkaForm) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.appForm.username");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.appForm.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty.appForm.confirmPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty.appTvrtkaForm.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.appTvrtkaForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contact", "NotEmpty.appTvrtkaForm.contact");

		// check user name
		if (!errors.hasFieldErrors("username")) {
			KorisnikENTITY korisnik2 = korisnikDAO.findAppUserByName( appTvrtkaForm.getUsername());
			if (korisnik2 != null) {
				// Username is not available.
				errors.rejectValue("username", "Duplicate.appForm.username");
			}
		}

		// check password
		if (!errors.hasErrors()) {
			if (!appTvrtkaForm.getConfirmPassword().equals(appTvrtkaForm.getPassword())) {
				errors.rejectValue("confirmPassword", "Match.appForm.confirmPassword");
			}
		}
	}
}
