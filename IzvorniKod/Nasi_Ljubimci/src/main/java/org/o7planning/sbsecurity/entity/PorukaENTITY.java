package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "poruka")
public class PorukaENTITY {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "serial")
	private int id;
	
	@Column(name = "datum", nullable = false,  columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date datum;
	
	@Column(name = "sadrzaj", nullable = true)
	private String sadrzaj;
	
	@ManyToOne
	@JoinColumn(name = "posiljatelj_username", nullable = false)
	private KorisnikENTITY posiljatelj;
	
	@ManyToOne
	@JoinColumn(name = "primatelj_username", nullable = false)
	private KorisnikENTITY primatelj;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	public KorisnikENTITY getPosiljatelj() {
		return posiljatelj;
	}

	public void setPosiljatelj(KorisnikENTITY posiljatelj) {
		this.posiljatelj = posiljatelj;
	}

	public KorisnikENTITY getPrimatelj() {
		return primatelj;
	}

	public void setPrimatelj(KorisnikENTITY primatelj) {
		this.primatelj = primatelj;
	}
}
