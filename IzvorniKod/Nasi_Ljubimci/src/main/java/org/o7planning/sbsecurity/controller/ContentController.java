package org.o7planning.sbsecurity.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import org.o7planning.sbsecurity.dao.KomentarDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_objDAO;
import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.PorukaDAO;
import org.o7planning.sbsecurity.dao.PrijavaDAO;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.entity.PorukaENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ContentController {

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private OsobaDAO osobaDAO;

	@Autowired
	private PorukaDAO porukaDAO;

	@Autowired
	private ObjavaDAO objavaDAO;

	@Autowired
	private KomentarDAO komentarDAO;

	@Autowired
	private Kor_kom_objDAO kor_kom_objDAO;

	@Autowired
	private PrijavaDAO prijavaDAO;

	// vraca stranicu za poruke
	@RequestMapping(value = "/poruke", method = RequestMethod.GET)
	public String dajPorukeStranica(Model model, Principal principal) {
		User korisnik = (User) ((Authentication) principal).getPrincipal();
		List<String> korisniciSKojimaPrica = porukaDAO.getKorisnike(korisnik.getUsername());
		model.addAttribute("korisnici", korisniciSKojimaPrica);
		return "poruke";
	}

	// vraca stranicu za poruke
	@RequestMapping(value = "/posaljiPoruku", method = RequestMethod.POST)
	public String posaljiPoruku(Model model, Principal principal, @RequestParam("username") String username,
			@RequestParam("poruka") String poruka) {
		if (poruka.length() < 1) {
			model.addAttribute("slanjeError", "Ne možete poslati praznu poruku");
			return "poruke";
		}

		if (!korisnikDAO.postoji(username)) {
			model.addAttribute("slanjeError", "Ne postoji navedeni korisnik");
			return "poruke";
		}
		User user = (User) ((Authentication) principal).getPrincipal();
		if (user.getUsername().equals(username)) {
			model.addAttribute("slanjeError", "Ne možete poslati poruku samome sebi");
			return "poruke";
		}

		// pohrani poruku
		porukaDAO.pohraniPoruku(user.getUsername(), username, poruka);

		return "redirect:/preusmjeriProfil";
	}

	// vraca stranicu sa svim porukama od korisnika
	// vraca stranicu za poruke
	@RequestMapping(value = "/prikaziPoruke", method = RequestMethod.POST)
	public String prikaziPoruke(Model model, Principal principal, @RequestParam("username") String username) {

		if (!korisnikDAO.postoji(username)) {
			model.addAttribute("prikazError", "ne postoji navedeni korisnik");
			return "poruke";
		}
		User user = (User) ((Authentication) principal).getPrincipal();
		if (user.getUsername().equals(username)) {
			model.addAttribute("prikazError", "ne mozete slati poruke samome sebi...");
			return "poruke";
		}

		// dohvati sve poruke
		List<PorukaENTITY> poruke = porukaDAO.dohvatiPoruke(user.getUsername(), username);
		String posiljatelj_username = poruke.get(0).getPosiljatelj().getUserName();
		// String primatelj_username = poruke.get(0).getPrimatelj().getUserName();

		// List<String> porukeString = WebUtils.pretvoriPoruke(poruke);

		model.addAttribute("poruke", poruke);
		model.addAttribute("vlasnik", username);
		model.addAttribute("posiljatelj", posiljatelj_username);
		return "pregledPoruka";
	}

	// vraca sliku ili video ovisno o id
	@RequestMapping(value = "/dajContent{id}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageAsByteArray(@PathVariable("id") Integer id) throws IOException {
		ObjavaENTITY objava = objavaDAO.dajObjavu(id);
		return ResponseEntity.ok().contentType(MediaType.valueOf(objava.getSlikaIliVideo())).body(objava.getData());
	}

	@RequestMapping(value = "/obrisiKomentar/{id}", method = RequestMethod.GET)
	public String obrisiKomentar(Model model, Principal principal, @PathVariable(name = "id") int id) {
		komentarDAO.obrisiKomentar(id);
		return ("redirect:/preusmjeriProfil");
	}

	@RequestMapping(value = "/obrisiObjavu/{id}", method = RequestMethod.GET)
	public String obrisiObjavu(Model model, Principal principal, @PathVariable(name = "id") int id) {
		objavaDAO.obrisiObjavu(id);
		return ("redirect:/preusmjeriProfil");

	}

	// upisuje komentar na danu objavu( id)
	@RequestMapping(value = "/komentirajObjavu{id}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void komentirajObjavu(Model model, @PathVariable("id") Integer id, @RequestParam("komentar") String komentar,
			Principal principal) {
		if (komentar == null || komentar.length() < 1) {
			return;
		}

		User user = (User) ((Authentication) principal).getPrincipal();
		String username = user.getUsername();

		// TODO ovo se moze optimirat, kad se sprema komentar id se automatski generira
		// pa ga se mozda moze odma i vratit
		komentarDAO.spremiKomentar(komentar);
		int komentarId = komentarDAO.najveciId();
		kor_kom_objDAO.spremiKomentar(username, id, komentarId);

	}

	// vraca sliku ili video ovisno o id
	@RequestMapping(value = "/dajProfilnu/{username}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageAsByteArray(@PathVariable("username") String username) throws IOException {
		OsobaENTITY vlasnik = osobaDAO.findOsobaByName(username);
		return ResponseEntity.ok().body(vlasnik.getProfilnaFotografija());
	}

	// vraca formu za prijavu korisnika
	@RequestMapping(value = "/prijavaKorisnika", method = RequestMethod.GET)
	public String prijavaFrom(Model model, Principal principal, @RequestParam("username") String username) {
		model.addAttribute("username", username); // username onog kojeg se prijavljuje
		return "prijavaForm";
	}

	@RequestMapping(value = "/prijavaKorisnika", method = RequestMethod.POST)
	public String prijavaKorisnika(Model model, Principal principal,
			@RequestParam("username") String usernamePrijavljenog, @RequestParam("opisPrijave") String opisPrijave) {
		User user = (User) ((Authentication) principal).getPrincipal();
		prijavaDAO.stvoriPrijavu(user.getUsername(), usernamePrijavljenog, opisPrijave);
		return "redirect:/preusmjeriProfil";
	}
}
