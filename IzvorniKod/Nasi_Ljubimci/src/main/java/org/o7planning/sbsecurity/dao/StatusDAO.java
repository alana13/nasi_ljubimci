package org.o7planning.sbsecurity.dao;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.o7planning.sbsecurity.entity.Zahtjev_za_prijateljstvoENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class StatusDAO {

	@Autowired
	private EntityManager entityManager;
	
	String tableName = "zahtjev_za_prijateljstvo";
	
	public String getStatus(String username1,String username2) {
		String sql = "select e from " + Zahtjev_za_prijateljstvoENTITY.class.getSimpleName() + " e " //
				+ " Where (e.osoba1.korisnik.userName = :username1 and e.osoba2.korisnik.userName= :username2) or (e.osoba1.korisnik.userName = :username2 and e.osoba2.korisnik.userName= :username1)";

		Query query = entityManager.createQuery(sql, Zahtjev_za_prijateljstvoENTITY.class);
		query.setParameter("username1", username1).setParameter("username2", username2);
		Zahtjev_za_prijateljstvoENTITY status = null;
		try {
			status = (Zahtjev_za_prijateljstvoENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("Query did not find any results");
		}
		if (status != null) {
			return status.getStatus();
		}
		else return null;
	}
	
	public boolean UpdateStatus(String username1, String username2, String status, String noviStatus) {
		if (status != null) {
			String sql1 = "update " + Zahtjev_za_prijateljstvoENTITY.class.getSimpleName()
					+ " e set e.osoba1.korisnik.userName= :username1, e.osoba2.korisnik.userName= :username2, e.status= :status where (e.osoba1.korisnik.userName = :username1 and e.osoba2.korisnik.userName= :username2) or (e.osoba1.korisnik.userName = :username2 and e.osoba2.korisnik.userName= :username1)";

			entityManager.createQuery(sql1).setParameter("status", noviStatus)
					.setParameter("username1", username1).setParameter("username2", username2).executeUpdate();
			return true;
		} else {
			String sql1 = "insert into " + tableName
					+ " (username_1 , username_2 , status) values (?,?,?)";
			entityManager.createNativeQuery(sql1).setParameter(1, username1).setParameter(2, username2)
					.setParameter(3, noviStatus).executeUpdate();
			return true;

		}
		
	}

	public boolean jesuLiPrijatelji(String username1, String username2) {
		
		String status= getStatus(username1, username2);
		if (status!=null) {
			if(status.compareTo("Friends")==0)
				return true;
		}
		
		return false;
	}

	public boolean DodajPrijatelja(String username1, String username2) {
		String status= getStatus(username1, username2);
		if(UpdateStatus(username1, username2, status,"Friends")==true)
			return true;
		else
			return false;
	}

	public boolean PosaljiZahtjev(String username1, String username2) {
		String status= getStatus(username1, username2);
		if(UpdateStatus(username1, username2, status,"Pending")==true)
			return true;
		else
			return false;

	}

	public boolean MakniPrijatelja(String username1, String username2) {
		String status= getStatus(username1, username2);
		if(UpdateStatus(username1, username2, status,"Unfriended")==true)
			return true;
		else
			return false;
	}
	
	public boolean BlokirajKorisnika(String username1,String username2) {
		String status= getStatus(username1, username2);
		if(UpdateStatus(username1, username2, status,"Blocked")==true)
			return true;
		else
			return false;
	}
	
	public boolean OdBlokirajKorisnika(String username1,String username2) {
		String status= getStatus(username1, username2);
		if(UpdateStatus(username1, username2, status,"Unfriended")==true)
			return true;
		else
			return false;
	}
	
	public List<String> sviZahtjevi(String username){
		String sql = "select e from " + Zahtjev_za_prijateljstvoENTITY.class.getName() + " e " //
				+ " Where (e.osoba2.korisnik.userName= :username) and e.status= :status";
		Query query = entityManager.createQuery(sql, Zahtjev_za_prijateljstvoENTITY.class);
		query.setParameter("username", username).setParameter("status", "Pending");
		List<Zahtjev_za_prijateljstvoENTITY> sviZahtjevi = null;
		try {
			sviZahtjevi = (List<Zahtjev_za_prijateljstvoENTITY>) query.getResultList();
		} catch (NoResultException e) {
			System.out.println("Query did not find any results");
		}
		List<String> sviKorisnici= new LinkedList<String>();
		for (Zahtjev_za_prijateljstvoENTITY zahtjev : sviZahtjevi) {
			if(zahtjev.getOsoba1().getKorisnik().getUserName().compareTo(username)!=0)
				sviKorisnici.add(zahtjev.getOsoba1().getKorisnik().getUserName());
			else if (zahtjev.getOsoba2().getKorisnik().getUserName().compareTo(username)!=0)
				sviKorisnici.add(zahtjev.getOsoba2().getKorisnik().getUserName());
			
		}
		return sviKorisnici;
	}
	
	public boolean korisnikJeBlokiran(String username1, String username2){
		String sql = "select e from " + Zahtjev_za_prijateljstvoENTITY.class.getName() + " e " //
				+ " Where e.osoba1.korisnik.userName = :username1 and e.osoba2.korisnik.userName= :username2 and status= :status";
		Query query = entityManager.createQuery(sql, Zahtjev_za_prijateljstvoENTITY.class);
		query.setParameter("username1", username1).setParameter("username2", username2).setParameter("status", "Blocked");
		Zahtjev_za_prijateljstvoENTITY status = null;
		try {
			status = (Zahtjev_za_prijateljstvoENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("Query did not find any results");
		}
		if(status== null) {
			return false;
		}
		return true;
	}
	
	public boolean korisnikVasJeBlokirao(String username1, String username2){
		String sql = "select e from " + Zahtjev_za_prijateljstvoENTITY.class.getName() + " e " //
				+ " Where e.osoba1.korisnik.userName = :username2 and e.osoba2.korisnik.userName= :username1 and status= :status";
		Query query = entityManager.createQuery(sql, Zahtjev_za_prijateljstvoENTITY.class);
		query.setParameter("username1", username1).setParameter("username2", username2).setParameter("status", "Blocked");
		Zahtjev_za_prijateljstvoENTITY status = null;
		try {
			status = (Zahtjev_za_prijateljstvoENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("Query did not find any results");
		}
		if(status== null) {
			return false;
		}
		return true;
	}
	
	public boolean jeliZahtjevPoslan(String username1, String username2) {
		String sql = "select e from " + Zahtjev_za_prijateljstvoENTITY.class.getName() + " e " //
				+ " Where e.osoba1.korisnik.userName = :username1 and e.osoba2.korisnik.userName= :username2 and status= :status";
		Query query = entityManager.createQuery(sql, Zahtjev_za_prijateljstvoENTITY.class);
		query.setParameter("username1", username1).setParameter("username2", username2).setParameter("status", "Pending");
		Zahtjev_za_prijateljstvoENTITY status = null;
		
		try {
			status = (Zahtjev_za_prijateljstvoENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("Zahtjev nije poslan");
		}
		try {
		System.out.println("Provjeravam je li zahtjev vec poslan, STATUS: " + status.getStatus());
		}catch(Exception e) {
			System.out.println("nope");
		}
		if(status== null) {
			return false;
		}
		return true;
	}
	
	public List<String> sviPrijatelji(String username){
		String sql = " Select username_1 from " + tableName + " " 
                + " WHERE username_2 = ? and status = ? "
				+ " UNION " 
				+ " Select username_2 from " + tableName + " " 
                + " WHERE username_1 = ? and status = ? ";
        
    	Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, username);
        query.setParameter(2, "Friends");
        query.setParameter(3, username);
        query.setParameter(4, "Friends");
        return (List<String>) query.getResultList();
	}
	
}
