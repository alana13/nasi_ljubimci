package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class Kor_kom_objPK implements Serializable{
	
	private KorisnikENTITY Korisnik;
	
	private KomentarENTITY Komentar;
	
	private ObjavaENTITY Objava;

	

	public Kor_kom_objPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

	public KomentarENTITY getKomentar() {
		return Komentar;
	}

	public void setKomentar(KomentarENTITY komentar) {
		Komentar = komentar;
	}

	public ObjavaENTITY getObjava() {
		return Objava;
	}

	public void setObjava(ObjavaENTITY objava) {
		Objava = objava;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Komentar, Korisnik, Objava);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kor_kom_objPK other = (Kor_kom_objPK) obj;
		return Objects.equals(Komentar, other.Komentar) && Objects.equals(Korisnik, other.Korisnik)
				&& Objects.equals(Objava, other.Objava);
	}
	
	
	
}
