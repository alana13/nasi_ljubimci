package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@IdClass(OsobaPK.class)
@Table(name = "Osoba")
public class OsobaENTITY {

	@Id
	@OneToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY korisnik;

	@Column(name = "ime", length = 36, nullable = false)
	private String firstname;

	@Column(name = "prezime", length = 36, nullable = false)
	private String userLastname;

	@Column(name = "email", length = 50, nullable = false)
	private String email;

	@Column(name = "profilna_fotografija", nullable = true)
	private byte[] profilnaFotografija;

	@Column(name = "opis", nullable = true)
	private String opis;

	@Column(name = "adresa", length = 50, nullable = true)
	private String adresa;

	@Column(name = "datum_registracije", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date datumRegistracije;

	public Date getDatumRegistracije() {
		return datumRegistracije;
	}

	public void setDatumRegistracije(Date datumRegistracije) {
		this.datumRegistracije = datumRegistracije;
	}

	public KorisnikENTITY getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		this.korisnik = korisnik;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getUserLastname() {
		return userLastname;
	}

	public void setUserLastname(String userLastname) {
		this.userLastname = userLastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte[] getProfilnaFotografija() {
		return profilnaFotografija;
	}

	public void setProfilnaFotografija(byte[] profilnaFotografija) {
		this.profilnaFotografija = profilnaFotografija;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
}