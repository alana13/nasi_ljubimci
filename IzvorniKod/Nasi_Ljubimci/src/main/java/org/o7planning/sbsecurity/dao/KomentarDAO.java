package org.o7planning.sbsecurity.dao;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class KomentarDAO {

	@Autowired
	private EntityManager entityManager;

	private static String tableName = "komentar";

	public boolean spremiKomentar(String komentar) {
		try {
			String sql = "insert into " + tableName + " (datum_i_vrijeme, sadrzaj) values(?, ?)";

			Date date = new Date(Calendar.getInstance().getTime().getTime());

			entityManager.createNativeQuery(sql).setParameter(1, date).setParameter(2, komentar).executeUpdate();
		} catch (NoResultException e) {
			return false;
		}

		return true;
	}

	public int najveciId() {
		try {
			String sql = "select max( id) from " + tableName;

			return (int) entityManager.createNativeQuery(sql).getSingleResult();
		} catch (NoResultException e) {
			return -1;
		}
	}

	public boolean obrisiKomentar(int komentarId) {
		try {

			String sql2 = "delete from "+Kor_kom_objENTITY.class.getSimpleName()+ " e where e.Komentar.id= :komentarId ";
			System.out.println(entityManager.createQuery(sql2)
					.setParameter("komentarId", komentarId)
					.executeUpdate());
		} catch (NoResultException e) {
			return false;
		}

		return true;
	}

}
