package org.o7planning.sbsecurity.entity;

import javax.persistence.Table;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "Dogadaj")
public class DogadajENTITY {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "serial")
	private int id;

	@Column(name = "datum_pocetak", nullable = false, columnDefinition = "TIMESTAMP")
	private LocalDateTime startDate;

	@Column(name = "datum_kraj", nullable = false, columnDefinition = "TIMESTAMP")
	private LocalDateTime endDate;

	@Column(name = "lokacija", nullable = false)
	private String lokacija;

	@Column(name = "opis", nullable = false)
	private String opis;

	@Column(name = "privpubl", nullable = false)
	private boolean privpubl;

	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getLokacija() {
		return lokacija;
	}

	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public boolean isPrivpubl() {
		return privpubl;
	}

	public void setPrivpubl(boolean privpubl) {
		this.privpubl = privpubl;
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

}
