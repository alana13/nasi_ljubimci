package org.o7planning.sbsecurity.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Objava")
public class ObjavaENTITY {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "serial")
	private int id;

	@Column(name = "opis", nullable = true)
	private String opis;

	@Column(name = "datum", nullable = false)
	private java.sql.Date datum;

	@Column(name = "slika_ili_video", nullable = true)
	private String SlikaIliVideo;

	@Column(name = "data", nullable = true)
	private byte[] data;
	
	@Column(name = "privpubl", nullable = false)
	private boolean privpubl;

	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private KorisnikENTITY Korisnik;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getSlikaIliVideo() {
		return SlikaIliVideo;
	}

	public void setSlikaIliVideo(String slikaIliVideo) {
		SlikaIliVideo = slikaIliVideo;
	}

	public boolean isPrivpubl() {
		return privpubl;
	}

	public void setPrivpubl(boolean privpubl) {
		this.privpubl = privpubl;
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		Korisnik = korisnik;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	
}
