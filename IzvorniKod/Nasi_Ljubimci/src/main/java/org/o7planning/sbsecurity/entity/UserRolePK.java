package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class UserRolePK implements Serializable {
	
	private KorisnikENTITY korisnik;
	
	private AppRoleENTITY approle;

	public UserRolePK() {
		super();
	}

	public KorisnikENTITY getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(KorisnikENTITY korisnik) {
		this.korisnik = korisnik;
	}

	public AppRoleENTITY getApprole() {
		return approle;
	}

	public void setApprole(AppRoleENTITY approle) {
		this.approle = approle;
	}

	@Override
	public int hashCode() {
		return Objects.hash(approle, korisnik);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRolePK other = (UserRolePK) obj;
		return Objects.equals(approle, other.approle) && Objects.equals(korisnik, other.korisnik);
	}
	

}
