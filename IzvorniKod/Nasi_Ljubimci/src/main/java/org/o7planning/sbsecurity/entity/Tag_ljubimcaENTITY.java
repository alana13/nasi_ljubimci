package org.o7planning.sbsecurity.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(Tag_ljubimcaPK.class)
@Table(name = "tag_ljubimca")
public class Tag_ljubimcaENTITY {

	@Id
	@ManyToOne
	@JoinColumn(name = "idLjub", nullable = false)
	private Profil_ljubimcaENTITY profilLjubimca;

	@Id
	@ManyToOne
	@JoinColumn(name = "idObj", nullable = false)
	private ObjavaENTITY objava;

	public Profil_ljubimcaENTITY getProfilLjubimca() {
		return profilLjubimca;
	}

	public void setProfilLjubimca(Profil_ljubimcaENTITY profilLjubimca) {
		this.profilLjubimca = profilLjubimca;
	}

	public ObjavaENTITY getObjava() {
		return objava;
	}

	public void setObjava(ObjavaENTITY objava) {
		this.objava = objava;
	}

	
}
