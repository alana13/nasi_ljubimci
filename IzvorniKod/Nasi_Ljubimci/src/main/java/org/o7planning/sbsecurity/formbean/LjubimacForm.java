package org.o7planning.sbsecurity.formbean;

import java.sql.Date;

public class LjubimacForm {
	
	private String vrsta;
	private String ime;
	private Date datum_rodenja;
	private String spol;
	private String opis;
	public String getVrsta() {
		return vrsta;
	}
	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Date getDatum_rodenja() {
		return datum_rodenja;
	}
	public void setDatum_rodenja(Date datum_rodenja) {
		this.datum_rodenja = datum_rodenja;
	}
	public String getSpol() {
		return spol;
	}
	public void setSpol(String spol) {
		this.spol = spol;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public LjubimacForm(String vrsta, String ime, Date datum_rodenja, String spol,  String opis) {
		super();
		this.vrsta = vrsta;
		this.ime = ime;
		this.datum_rodenja = datum_rodenja;
		this.spol = spol;
		this.opis = opis;
	}
	
	
	
	
	
	
	

}
