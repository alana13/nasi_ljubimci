package org.o7planning.sbsecurity.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(Osoba_dogPK.class)
@Table(name = "osoba_dog")
public class Osoba_dogENTITY {
	
	
	@Id
	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private OsobaENTITY Osoba;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "id", nullable = false)
	private DogadajENTITY Dogadaj;

	public OsobaENTITY getKorisnik() {
		return Osoba;
	}

	public void setKorisnik(OsobaENTITY Osoba) {
		this.Osoba = Osoba;
	}

	public DogadajENTITY getDogadaj() {
		return Dogadaj;
	}

	public void setDogadaj(DogadajENTITY dogadaj) {
		Dogadaj = dogadaj;
	}
	

}
