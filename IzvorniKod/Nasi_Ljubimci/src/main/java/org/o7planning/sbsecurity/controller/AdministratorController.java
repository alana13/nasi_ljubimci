package org.o7planning.sbsecurity.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.o7planning.sbsecurity.dao.BansDAO;
import org.o7planning.sbsecurity.dao.Kor_kom_objDAO;
import org.o7planning.sbsecurity.dao.KorisnikDAO;
import org.o7planning.sbsecurity.dao.LjubimacDAO;
import org.o7planning.sbsecurity.dao.ObjavaDAO;
import org.o7planning.sbsecurity.dao.OsobaDAO;
import org.o7planning.sbsecurity.dao.PrijavaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaDAO;
import org.o7planning.sbsecurity.dao.TvrtkaUslugaDAO;
import org.o7planning.sbsecurity.entity.Kor_kom_objENTITY;
import org.o7planning.sbsecurity.entity.ObjavaENTITY;
import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.entity.PrijavaENTITY;
import org.o7planning.sbsecurity.entity.Profil_ljubimcaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaENTITY;
import org.o7planning.sbsecurity.entity.TvrtkaUslugaENTITY;
import org.o7planning.sbsecurity.models.Administrator;
import org.o7planning.sbsecurity.utils.TvrtkaUtils;
import org.o7planning.sbsecurity.utils.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administrator")
public class AdministratorController {

	@Autowired
	private Kor_kom_objDAO kor_kom_objDAO;

	@Autowired
	private KorisnikDAO korisnikDAO;

	@Autowired
	private ObjavaDAO objavaDAO;

	@Autowired
	private OsobaDAO osobaDAO;

	@Autowired
	private PrijavaDAO prijavaDAO;

	@Autowired
	private TvrtkaDAO tvrtkaDAO;

	@Autowired
	private TvrtkaUslugaDAO tvrtkaUslugaDAO;

	@Autowired
	private BansDAO bansDAO;
	
	@Autowired
	private LjubimacDAO ljubimacDAO;
	
	// glavna stranica: profil administratora
	@RequestMapping(value = "/profil", method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {
		// dohvati sve prijave
		List<PrijavaENTITY> prijave = prijavaDAO.dohvatiPrijave();
		model.addAttribute("prijave", prijave);

		return "administratorProfil";
	}

	// vraca sve podatke potrebne za pregled drugog profila
	@RequestMapping(value = "/pregledDrugogProfila", method = RequestMethod.POST)
	public String pregledProfila(Model model, Principal principal, @RequestParam("username") String username,
			RedirectAttributes redirAttr) {
		if (username == null || username.trim().length() < 1) {
			redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
			return "redirect:/administrator/profil";
		}

		if (tvrtkaDAO.postoji(username)) {
			// Ako je uneseno ime tvrtke

			model.addAttribute("username", username);

			// dohvati informacije o tvrtki
			// podaci
			TvrtkaENTITY podaci = tvrtkaDAO.pronadi(username);
			// usluge
			List<TvrtkaUslugaENTITY> usluge = tvrtkaUslugaDAO.dajUsluge(username);
			Map<String, String> informacije = TvrtkaUtils.mapirajUINformacije(podaci, usluge);
			model.addAttribute("informacije", informacije);

			// dohavati sve objave od tvtke
			List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(username);
			model.addAttribute("sveObjave", sveObjave);

			// dohvati sve komentare
			// dohavti komentare na objave
			List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
			Map<Integer, List<Kor_kom_objENTITY>> komentari = WebUtils.komentarIzObjektaUString(komentariObjekti);
			model.addAttribute("komentari", komentari);
			return "pregledTvrtke";
		} else if (osobaDAO.postoji(username)) {
			// Ako je uneseno ime osobe

			model.addAttribute("username", username);

			// dohvati podatke
			OsobaENTITY podaci = osobaDAO.findOsobaByName(username);
			model.addAttribute("podaci", podaci);
			
			// dohavati sve objave od vlasnika
			List<ObjavaENTITY> sveObjave = objavaDAO.dajSveObjaveObjava(username);
			model.addAttribute("sveObjave", sveObjave);

			// dohvati sve komentare
			// dohavti komentare na objave
			List<Kor_kom_objENTITY> komentariObjekti = kor_kom_objDAO.dajSveKomentareNaObjave(sveObjave);
			Map<Integer, List<Kor_kom_objENTITY>> komentari = WebUtils.komentarIzObjektaUString(komentariObjekti);
			model.addAttribute("komentari", komentari);
						
			List<Profil_ljubimcaENTITY> ljubimci = ljubimacDAO.listAllLjubimci( username);
			model.addAttribute("sviLjubimci", ljubimci);
			return "administratorPregledavaVlasnika";
		}
		// Ako ne postoji taj korisnik
		redirAttr.addFlashAttribute("pretrazivanjeError", "ne postoji korisnik s unesenim imenom");
		return "redirect:/administrator/profil";
	}

	// Blokira prijavljenog korisnika
	@RequestMapping(value = "/blokirajKorisnika", method = RequestMethod.POST)
	public String blokirajKorisnika(Model model, Principal principal,
			@RequestParam("username") String username,
			@RequestParam("idPrijave") Integer idPrijave,
			@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
			@RequestParam("banUntil") LocalDateTime banUntil) {
		bansDAO.banUser(username, banUntil);
		prijavaDAO.izbrisiPrijavu( idPrijave);
		return "redirect:/administrator/profil";
	}

	// Odbacuje prijavu
	@RequestMapping(value = "/odbaciPrijavu", method = RequestMethod.POST)
	public String odbaciPrijavu(Model model, Principal principal, @RequestParam("idPrijave") Integer idPrijave) {
		prijavaDAO.izbrisiPrijavu( idPrijave);
		return "redirect:/administrator/profil";
	}

	// Brise korisnika iz baze
//	@RequestMapping(value = "/obrisiKorisnika{username}", method = RequestMethod.POST)
//	public String obrisiKorisnika(Model model, Principal principal,
//			@RequestParam("idPrijave") Integer idPrijave,
//			@RequestParam("username") String username) {
//		prijavaDAO.izbrisiPrijavu( idPrijave);
//		korisnikDAO.obrisiKorisnika( username);
//		return "redirect:/administrator/profil";
//	}
}