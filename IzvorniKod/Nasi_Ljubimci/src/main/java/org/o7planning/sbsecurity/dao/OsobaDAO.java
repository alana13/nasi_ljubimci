package org.o7planning.sbsecurity.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.o7planning.sbsecurity.entity.OsobaENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.o7planning.sbsecurity.formbean.VlasnikUpdateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class OsobaDAO {
	@Autowired
	private EntityManager entityManager;

	private static String tableName = "Osoba";

	public OsobaENTITY findOsobaByName(String userName) {
		try {
			String sql = "Select e from " + OsobaENTITY.class.getName() + " e " //
					+ " Where e.korisnik.userName = :userName ";

			Query query = entityManager.createQuery(sql, OsobaENTITY.class);
			query.setParameter("userName", userName);

			return (OsobaENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public OsobaENTITY findOsobaByEmail(String email) {
		try {
			String sql = "Select e from " + OsobaENTITY.class.getName() + " e " //
					+ " Where e.email = :email ";

			Query query = entityManager.createQuery(sql, OsobaENTITY.class);
			query.setParameter("email", email);

			return (OsobaENTITY) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean createOsoba(AppOsobaForm form) {

		try {
			String sql = "insert into " + tableName
					+ " (username, ime, prezime, profilna_fotografija, email) values(?, ?, ?, NULL, ?)";

			entityManager.createNativeQuery(sql).setParameter(1, form.getUsername())
					.setParameter(2, form.getFirstName()).setParameter(3, form.getLastName())
					.setParameter(4, form.getEmail()).executeUpdate();
		} catch (NoResultException e) {
			return false;
		}

		return false;
	}

	public boolean postoji(String username) {
		String sql = "select e from " + OsobaENTITY.class.getName() + " e " //
				+ " Where e.korisnik.userName = :arg ";

		Query query = entityManager.createQuery(sql, OsobaENTITY.class);
		query.setParameter("arg", username);

		return (boolean) (query.getResultList().size() > 0);
	}

	public boolean azurirajPodatke(String username, VlasnikUpdateForm form) throws IOException {
		if(form.getProfilnaFotografija().isEmpty()) {
			return azurirajPodatkeBezProfilne(username, form);
		}
		
		try {
            String sql = "update " + OsobaENTITY.class.getName() +
            		" e SET e.adresa = :adresa, e.opis= :opis, e.email= :email, e.profilnaFotografija= :profilnaFotografija " +
            		" where e.korisnik.userName = :username";
            
            entityManager.createQuery(sql)
            	.setParameter("adresa", form.getAdresa())
            	.setParameter("opis", form.getOpis())
            	.setParameter("email", form.getEmail())
            	.setParameter("profilnaFotografija", form.getProfilnaFotografija().getBytes())
            	.setParameter("username", username)
            	.executeUpdate();
        } catch(NoResultException e) {
            return false;
        }
		
    	return true;
	}

	public boolean azurirajPodatkeBezProfilne(String username, VlasnikUpdateForm form) {
		try {
            String sql = "update " + OsobaENTITY.class.getName() +
            		" e SET e.adresa = :adresa, e.opis= :opis, e.email= :email " +
            		" where e.korisnik.userName = :username";
            
            entityManager.createQuery(sql)
            	.setParameter("adresa", form.getAdresa())
            	.setParameter("opis", form.getOpis())
            	.setParameter("email", form.getEmail())
            	.setParameter("username", username)
            	.executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	return true;
	}
	
	public List<String> dajPrijatelje( String username){
		return null;
	}
}

