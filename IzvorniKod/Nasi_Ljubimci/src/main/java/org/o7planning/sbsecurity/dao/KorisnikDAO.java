package org.o7planning.sbsecurity.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.annotations.SQLDelete;
import org.o7planning.sbsecurity.entity.KorisnikENTITY;
import org.o7planning.sbsecurity.formbean.AppOsobaForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class KorisnikDAO {
	
	@Autowired
    private EntityManager entityManager;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
    private static String tableName = "Korisnik";
    
	public KorisnikENTITY findAppUserByName(String userName) {
        try {
            String sql = "Select e from " + KorisnikENTITY.class.getName() + " e " //
                    + " Where e.userName = :userName ";

            Query query = entityManager.createQuery(sql, KorisnikENTITY.class);
            query.setParameter("userName", userName);

            return (KorisnikENTITY) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
	
	public boolean postoji( String username) {
		return findAppUserByName( username) != null;
	}
	
	public boolean createKorisnik( String username, String password) {
    	String encryptedPassword = this.passwordEncoder.encode( password);
    	
    	try {
            String sql = "insert into " + tableName + " (username, password, enabled) values (?, ?, ?)";
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, username)
            .setParameter(2, encryptedPassword)
            .setParameter(3, 1)
            .executeUpdate();
        } catch (NoResultException e) {
            return false;
        }
    	
    	return false;
    }
    
    public boolean obrisiKorisnika(String username) {
    	try {
            String sql = "DELETE FROM " + KorisnikENTITY.class.getName() +
                        " e WHERE username = :username";
            
            entityManager.createQuery(sql).setParameter("username", username).executeUpdate();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    };
}
