package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class Osoba_dogPK implements Serializable{

	
	private OsobaENTITY Osoba;
	
	private DogadajENTITY Dogadaj;

	

	public Osoba_dogPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OsobaENTITY getOsoba() {
		return Osoba;
	}

	public void setOsoba(OsobaENTITY osoba) {
		Osoba = osoba;
	}

	public DogadajENTITY getDogadaj() {
		return Dogadaj;
	}

	public void setDogadaj(DogadajENTITY dogadaj) {
		Dogadaj = dogadaj;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Dogadaj, Osoba);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Osoba_dogPK other = (Osoba_dogPK) obj;
		return Objects.equals(Dogadaj, other.Dogadaj) && Objects.equals(Osoba, other.Osoba);
	}
	
	
}
