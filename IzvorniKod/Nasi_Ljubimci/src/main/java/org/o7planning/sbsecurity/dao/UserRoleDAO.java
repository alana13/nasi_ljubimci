package org.o7planning.sbsecurity.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserRoleDAO {

	@Autowired
    private EntityManager entityManager;
	
	private static String tableName = "userrole"; 
	
	public boolean createUserRole( String userName, int roleId) {	
    	try {
            String sql = "insert into " + tableName + " (username, roleid) values (?, ?)";
            
            entityManager.createNativeQuery( sql)
            .setParameter(1, userName)
            .setParameter(2, roleId)
            .executeUpdate();
        } catch (NoResultException e) {
            System.out.println( "nije se uspio zapisati odnos korisnika i uloge");
            e.printStackTrace();
        	return false;
        }
    	
    	return true;
    }
	
	public List<String> getRoleNames(String username) {
        String sql = "select approle.rolename from userrole join approle on userrole.roleId = approle.roleId"
        		+ " where userrole.username = ? ";
        
        List<String> result = entityManager.createNativeQuery( sql)
        						.setParameter(1, username)
        						.getResultList();
        
        return result;
    }
}
