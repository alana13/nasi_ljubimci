package org.o7planning.sbsecurity.entity;

import java.io.Serializable;
import java.util.Objects;

public class Kor_kom_dogPK implements Serializable {
	
	private KorisnikENTITY Korisnik;
	
	private KomentarENTITY Komentar;
	
	private DogadajENTITY Dogadaj;



	public Kor_kom_dogPK() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikENTITY getKorisnik() {
		return Korisnik;
	}

	public void setKorisnik(KorisnikENTITY Korisnik) {
		this.Korisnik = Korisnik;
	}

	public KomentarENTITY getKomentar() {
		return Komentar;
	}

	public void setKomentar(KomentarENTITY Komentar) {
		this.Komentar = Komentar;
	}

	public DogadajENTITY getDogadaj() {
		return Dogadaj;
	}

	public void setDogadaj(DogadajENTITY Dogadaj) {
		this.Dogadaj = Dogadaj;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Dogadaj, Komentar, Korisnik);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kor_kom_dogPK other = (Kor_kom_dogPK) obj;
		return Objects.equals(Dogadaj, other.Dogadaj) && Objects.equals(Komentar, other.Komentar)
				&& Objects.equals(Korisnik, other.Korisnik);
	}
	
	

}
