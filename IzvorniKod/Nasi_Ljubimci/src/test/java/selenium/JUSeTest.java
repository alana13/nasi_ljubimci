package selenium;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JUSeTest {

	private WebDriver driver;
	private WebElement element;

	public static final String loginURL = "https://nasi-ljubimci1.herokuapp.com/login";
	public static final String registerVlasnikURL = "https://nasi-ljubimci1.herokuapp.com/registerOsoba";

	@BeforeEach
	public void inicijalizacija() {
		driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterEach
	public void zatvoriProzor() {
		driver.quit();
	}

	@Test
	public void dobarLogin() {
		// pretpostavka: postoji korisnik s username = renato i password = 123
		driver.get(loginURL);

		element = driver.findElement(By.name("username"));
		element.sendKeys("renato");

		element = driver.findElement(By.name("password"));
		element.sendKeys("123");

		driver.findElement(By.cssSelector("input[type='submit']")).click();

		String redirURL = driver.getCurrentUrl();
		boolean proslo = redirURL.endsWith("vlasnik/profil");
		assertEquals(true, proslo, "nece se ulogirat za dobar input");
	}

	@Test
	public void kriviLogin() {
		// pretpostavka: postoji korisnik s username = renato, ali mu sifra nije 1234
		driver.get(loginURL);

		element = driver.findElement(By.name("username"));
		element.sendKeys("renato");

		element = driver.findElement(By.name("password"));
		element.sendKeys("1234");

		driver.findElement(By.cssSelector("input[type='submit']")).click();

		String redirURL = driver.getCurrentUrl();
		boolean zaustavljeno = redirURL.startsWith(loginURL); // na kraju su path variable za error
		assertEquals(true, zaustavljeno, "proso login s krivim vjerodajnicama");
	}

	@Test
	public void registracijaZauzetUsername() {
		// pretpostavka: vec postoji korisinik s username = renato
		driver.get(registerVlasnikURL);

		element = driver.findElement(By.name("username"));
		element.sendKeys("renato");

		element = driver.findElement(By.name("password"));
		element.sendKeys("123");

		element = driver.findElement(By.name("confirmPassword"));
		element.sendKeys("123");

		element = driver.findElement(By.name("email"));
		element.sendKeys("nesto@fer.hr");

		element = driver.findElement(By.name("firstName"));
		element.sendKeys("Pero");

		element = driver.findElement(By.name("lastName"));
		element.sendKeys("Peric");

		driver.findElement(By.cssSelector("input[type='submit']")).click();

		String redirURL = driver.getCurrentUrl();
		boolean zaustavljeno = redirURL.startsWith(registerVlasnikURL);
		assertEquals(true, zaustavljeno, "proso proso login s vec postojecim username-mom");
	}

	@Test
	public void registracijaKriviConfirmPassword() {
		// pretpostavka: ne postoji korisnik s username = nesto ili email = nesto@fer.hr
		driver.get(registerVlasnikURL);

		element = driver.findElement(By.name("username"));
		element.sendKeys("nesto");

		element = driver.findElement(By.name("password"));
		element.sendKeys("123");

		element = driver.findElement(By.name("confirmPassword"));
		element.sendKeys("1234");

		element = driver.findElement(By.name("email"));
		element.sendKeys("nesto@fer.hr");

		element = driver.findElement(By.name("firstName"));
		element.sendKeys("Pero");

		element = driver.findElement(By.name("lastName"));
		element.sendKeys("Peric");

		driver.findElement(By.cssSelector("input[type='submit']")).click();

		String redirURL = driver.getCurrentUrl();
		boolean zaustavljeno = redirURL.startsWith(registerVlasnikURL);
		assertEquals(true, zaustavljeno, "proso proso login s krivo potvrdenom sifrom");
	}
	
}
