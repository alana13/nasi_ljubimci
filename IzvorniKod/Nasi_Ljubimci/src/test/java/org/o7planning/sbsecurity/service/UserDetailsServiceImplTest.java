package org.o7planning.sbsecurity.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.o7planning.sbsecurity.models.Administrator;
import org.o7planning.sbsecurity.models.Tvrtka;
import org.o7planning.sbsecurity.models.Vlasnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SpringBootTest
class UserDetailsServiceImplTest {

	@Autowired
	UserDetailsServiceImpl userDetailsServiceImpl;

	@Test
	public void vlasnikLogin() {
		// u bazi bi trebao biti spremljen vlasnik s imenom renato
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername("renato");
		boolean jelVlasnik = userDetails.getClass().equals(Vlasnik.class);
		assertEquals(true, jelVlasnik, " uneseni user bi trebao biti vlasnik");
	}

	@Test
	public void tvrtkaLogin() {
		// u bazi bi trebala biti spremljena tvrtka s imenom tvrtko
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername("tvrtko");
		boolean jelTvrtka = userDetails.getClass().equals(Tvrtka.class);
		assertEquals(true, jelTvrtka, " uneseni user bi trebala biti tvrtka");
	}

	@Test
	public void adminLogin() {
		// u bazi bi trebao biti spremljen admin a imenom dbadmin1
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername("admin");
		boolean jelAdmin = userDetails.getClass().equals(Administrator.class);
		assertEquals(true, jelAdmin, " uneseni user bi trebao biti admin");
	}
	
	@Test
	public void nullUser() {
		// u baze ne bi trebao biti spremljen korisnik s username = nista
		assertThrows( UsernameNotFoundException.class, ()-> {userDetailsServiceImpl.loadUserByUsername("nista");},
				"ne izbacuje error za nepoznatog usera");
	}
}
